#importing the required packages

import smtplib
import json
from urllib2 import Request, urlopen
import sys
from pyspark import SparkContext
from pyspark import SparkConf
import os

#spark-submit /home/engineering/prasanna/users_usersassert.py resultantPath targetPath

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################


if len(sys.argv) != 3 and len(sys.argv) != 4 and len(sys.argv) != 5:
    print >> sys.stderr, "Usage: " + sys.argv[0] + " <resultantPath> <targetPath> [<limit> / <start value> <end value>]"
    exit(-1)

# Store the arguments into variables
####################################

if len(sys.argv) == 3:
    script, resultantPath, targetPath = sys.argv

if len(sys.argv) == 4:
    script, path1, path2, limit_count = sys.argv

if len(sys.argv) == 5:
    script, path1, path2, start_value, end_value = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]

### Spark Configuration for executing the program ###
#***************************************************#

conf = (SparkConf().setMaster("local[*]").setAppName("comparision of two rdds").set("spark.executor.memory", "1g"))
sc = SparkContext(conf = conf)

#comparison of ods and target textfiles from the hdfs location

resultRDD = sc.textFile(resultantPath).map(lambda s:s.encode('ascii','ignore').split("\t")).map(lambda s:s[2:])
expectedRDD = sc.textFile(targetPath).map(lambda s:s.encode('ascii','ignore').split("\t")).map(lambda s:s[2:])

result_take = resultRDD.take(1)
expected_take = expectedRDD.take(1)

lengthOfList1 = len(result_take[0])
lengthOfList2 = len(expected_take[0])

if lengthOfList1 == lengthOfList2:

   for i in range(lengthOfList1):

      if resultRDD.map(lambda x: x[i]).collect() == expectedRDD.map(lambda x: x[i]).collect():
         print "******************************************************************************************************************************************"
         print "in "+resultantPath+ " and "+targetPath+" "+ str(i+3) + "th column is as expected"
         print "******************************************************************************************************************************************"
      else:
         print "******************************************************************************************************************************************"
         print "in "+resultantPath+ " and "+targetPath+" "+ str(i+3) + "th column results are different"
         print "******************************************************************************************************************************************"
         break 
else:
   print "************************************************************************************************************************************************"
   print "in "+resultantPath+ " and "+targetPath+" no of tab seperated fields are different"
   print "************************************************************************************************************************************************"


