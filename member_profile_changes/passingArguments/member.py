#USAGE OF .PY FILE
#spark-submit /home/engineering/passingArguments/member.py resultantPath targetPath

#importing the required packages
import smtplib
import json
from urllib2 import Request, urlopen
import sys
from pyspark import SparkContext
from pyspark import SparkConf
import os
import commands
from pyspark.sql import HiveContext
from hdfs3 import HDFileSystem
import hipchat

### Spark Configuration for executing the program ###
#***************************************************#

conf = (SparkConf().setMaster("local[*]").setAppName("comparision of two rdds").set("spark.executor.memory", "1g"))
sc = SparkContext(conf = conf)
sqlContext = HiveContext(sc)


#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################


if len(sys.argv) != 3 and len(sys.argv) != 4 and len(sys.argv) != 5:
    print >> sys.stderr, "Usage: " + sys.argv[0] + " <resultantPath> <targetPath> [<limit> / <start value> <end value>]"
    exit(-1)

# Store the arguments into variables
####################################

if len(sys.argv) == 3:
    script, resultantPath, targetPath = sys.argv

if len(sys.argv) == 4:
    script, resultantPath, targePath, limit_count = sys.argv

if len(sys.argv) == 5:
    script, resultantPath, targetPath, start_value, end_value = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]

#comparison of dw original and target textfiles from the hdfs location

resultDF = sqlContext.read.parquet(resultantPath)
expectedDF = sqlContext.read.parquet(targetPath)

no_of_columns = len(resultDF.columns)

#for comparing the number of partitions in the hdfs location
resultFilesCount = commands.getoutput('hadoop fs -ls '+resultantPath+'/* | wc -l')
targetFilesCount = commands.getoutput('hadoop fs -ls '+targetPath+'/* | wc -l')

if resultFilesCount == targetFilesCount:
   print " "+resultantPath+" parquet files count is = "+str(resultFilesCount)+" "+targetPath+" parquet files count is = "+str(targetFilesCount)+" "
else:
   print " "+resultantPath+" parquet files count is = "+str(resultFilesCount)+" "+targetPath+" parquet files count is = "+str(targetFilesCount)+" "
   hipchat.hipchatMessage('@job3_auto_test : it is failed to compare the number of parquet files in two hdfs locations data')
   sys.exit()

#Comparing the Parquet Files with sorting order of the data in files
resultdfparquet = sqlContext.read.parquet(resultantPath+'/part-r-00000*')
targetdfparquet = sqlContext.read.parquet(targetPath+'/part-r-00000*')

if resultdfparquet.select(resultdfparquet[8]).collect() == targetdfparquet.select(targetdfparquet[8]).collect():
   print "parquet files data is in the sorting order"
else:
   print "parquet files are not in the sorting order"
   hipchat.hipchatMessage('@job3_auto_test : in hdfs /user/hue/target location parquet files data is not in the sorting order')
   sys.exit()

#Configure the host name of the hdfs file system
hdfs = HDFileSystem(host='bigdata.dealwallet.com', port=8020)
size_comp_target = hdfs.du('/user/hue/target/', total=False, deep=False)
convert_list_size = list(size_comp_target)
convert_list_size.sort()
no_of_files = len(convert_list_size)

for i in range(no_of_files):
   if i != no_of_files:
      size = size_comp_target[convert_list_size[i]]
      if 25000 < size < 40000:
         print "Size of file number "+ str(i) +" is "+ str(size)
      else:
         print str(i)+" Nunber file sizes  are different and size = "+str(size)
         hipchat.hipchatMessage(str(i)+' number File sizes in the location '+resultantPath+' are different and size = '+str(size))
#         sys.exit(9999)

for i in range(no_of_columns):
   if i not in [6, 7, 41, 42, 96]:
       if resultDF.select(resultDF[i]).collect() == expectedDF.select(expectedDF[i]).collect():
          print "In "+resultantPath+" and "+targetPath+" "+str(i)+ "th column is as expected"
       else:
          print "In data validation an error occured in column number "+str(i)+" from count starts with 0"
          hipchat.hipchatMessage('In data validation an error occured in column number '+str(i)+" from count starts with 0")
          sys.exit()

