#!/usr/bin/env python
import json
from urllib2 import Request, urlopen
import ConfigParser
import sys
import os

config = ConfigParser.RawConfigParser()
config.read('config.properties')

user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   Room_id = config.get('Properties','Room_id')
   Room_name = config.get('Properties','Room_name')

V2TOKEN = 'dALuFF3hciryIf0mwPI98rgcVyHhoIwzUwC7NJ4z'
#ROOMID = 1181440
link = open("linkfile.log","r").read()
# API V2, send message to room:
url = 'https://api.hipchat.com/v2/room/%s/notification' % Room_id
print link
headers = {
    "content-type": "application/json",
    "authorization": "Bearer %s" % V2TOKEN}
datastr = json.dumps({
    'message': 'Follow this link for member_profile job log file:    %s' %link,
    'color': 'red',
    'message_format': 'html',
    'notify': True})
request = Request(url, headers=headers, data=datastr)
uo = urlopen(request)
rawresponse = ''.join(uo)
uo.close()
assert uo.code == 204
print 'hipchat notification log_link send successfully to '+Room_name
