import psycopg2
import sys
import os

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
#conn = psycopg2.connect(database="bigdata", user="dealwallet", password="infotech369", host="localhost", port="5432")
print "Opened bigdata database successfully"

cur = conn.cursor()

#DROP THE TABLE payments
cur.execute('''drop table payments;''')
conn.commit()
print "successfully drop the table payments"

#CREATE A TABLE payments

cur.execute('''create table if not exists payments(upo_user_id integer,upo_payment_group_id smallint,upo_payment_method_id smallint,upo_third_party boolean,upo_payment_email_address character varying,upo_last_modified timestamp without time zone ,source_ip_address inet,source_country text,upo_yandex_id character varying);''')
conn.commit()
print "payments table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE payments" 

for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*180000
      for i in [x]:
         cur.execute('''insert into payments (upo_user_id,upo_payment_group_id,upo_payment_method_id,upo_third_party,upo_payment_email_address,upo_last_modified,source_ip_address,source_country,upo_yandex_id) values (%s,1,1,'f','ram%s@gmail.com',current_date,'70.208.3.1/32','india',' ')'''%(i,i))
         conn.commit()
print "values successfully inserted into payments"


#TRUNCATE THE TABLE COMMAND
#cur.execute('''truncate table payments;''')
#conn.commit()
#print "successfully truncated table payments"
