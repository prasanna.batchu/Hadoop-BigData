import psycopg2
import sys
import os

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################

if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user = os.environ["USER"]

#conn = psycopg2.connect(database="bigdata",user="dealwallet",password="infotech369",host="localhost",port="5432")
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened database bigdata successfully"

#drop table users_info
cur = conn.cursor()
cur.execute('''drop table users_info;''')
conn.commit()
print "successfully drop table users_info"

#CREATE A TABLE USERS_INFO

cur.execute('''create table if not exists users_info (ui_id numeric,ui_user_id numeric,ui_firstname character varying,ui_lastname character varying,ui_address1 character varying,ui_address2 character varying,ui_postcode character varying,ui_country_id integer,ui_city character varying,ui_state_id integer,ui_province character varying,ui_last_modified timestamp without time zone,ui_barcode character varying,ui_record_type character varying,source_ip_address inet,source_country text,browser_language text);''')
conn.commit()
print "users_info table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE USERS_INFO" 

for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*180000
      for i in [x]:
         cur.execute('''insert into users_info (ui_id,ui_user_id,ui_firstname,ui_lastname,ui_address1,ui_address2,ui_postcode,ui_country_id,ui_city,ui_state_id,ui_province,ui_last_modified,ui_barcode,ui_record_type,source_ip_address,source_country,browser_language) values ('%s','%s','anji%s','batchu%s','1-1%s modumudi avg','','52112%s',461471,'machilipatnam%s','8%s','','2016-09-01 07:07:49.738','4%s','S','69.121.190.97','India','en-US')'''%(i,i,i,i,i,i,i,i,i))  
         conn.commit()
print "values successfully inserted"

