import psycopg2
import sys
import os
import ConfigParser

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

config = ConfigParser.RawConfigParser()
config.read('config.properties')
user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   partition_range = config.get('Properties','partition_range')

#conn = psycopg2.connect(database="bigdata", user="dealwallet", password="infotech369", host="localhost", port="5432")
conn = psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened database successfully"

cur = conn.cursor()
# drop table users_users
cur.execute('''drop table users_users;''')
conn.commit()
print "successfully drop table users_users"
#CREATE A TABLE USERS_USERS
cur.execute('''create table if not exists users_users (user_id numeric,user_email character varying(255),user_password character varying,user_active boolean,user_email_valid boolean,user_creation_date timestamp with time zone,user_last_modified timestamp with time zone,user_registration_type character(3),user_partner_token character varying(255),password_status_id integer,toolbar_status boolean,gender character varying(1),referral_token text,ge_credit_card_pa_acceptance_code character varying,ge_credit_card_pa_product_type character varying,ge_credit_card_pa_expiration_ts character varying,ge_credit_card_pa_approval_status character varying,ge_credit_card_pa_approval_ts character varying,referrer_type_id integer,referral_shorturl character varying,password character varying(100),locale character varying(5),has_short_url boolean,ge_credit_card_pa_start_ts character varying);''')
conn.commit()
print "users_users table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO COLUMN USER_ID
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE USERS_USERS"

#def values(start_value,end_value):
for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*int(partition_range)
      for i in [x]:
         cur.execute('''insert into users_users (user_id,user_email,user_password,user_active,user_email_valid,user_creation_date,user_last_modified,user_registration_type,user_partner_token,password_status_id,toolbar_status,gender,referral_token,ge_credit_card_pa_acceptance_code,ge_credit_card_pa_product_type,ge_credit_card_pa_expiration_ts ,ge_credit_card_pa_approval_status,ge_credit_card_pa_approval_ts,referrer_type_id,referral_shorturl,password,locale,has_short_url,ge_credit_card_pa_start_ts) values ('%s','prasanna%s%s@gmail.com','anj$ne&ya%s','T','T',current_date,current_date,'CSB','','%s','F','F','','','','','','','%s','','','','T','')'''%(i,i,i,i,i,i))
         conn.commit()
print "successfully inserted in to the table USERS_USERS"

#TRUNCATE THE TABLE COMMAND
#cur.execute('''truncate table users_users;''')
#conn.commit()
#print "successfully truncated table users_users"


#DROP THE TABLE USER_INFO
#cur.execute('''drop table users_users;''')
#conn.commit()
#print "successfully drop the table users_users"
