#!/bin/bash
. /home/engineering/member_profile/config.properties
ods_inserted_ts=$(TZ='America/Los_Angeles' date +'%Y-%m-%d %H:%M:%S.%6N-07:00')
echo $ods_inserted_ts
ods_inserted_ts_utc=$(TZ='Etc/UTC' date +'%Y-%m-%d %H:%M:%S.%6N+00:00')
echo $ods_inserted_ts_utc

sqoop job --create users_user_hold_payment -- import -m 1  --connect "$db_admin_url" --username postgres --password postgres123 --query "select '$ods_inserted_ts'::text,'$ods_inserted_ts_utc'::text as utc,id,user_id,hold_payment,modified_ts from users_user_hold_payment where  \$CONDITIONS" --incremental append -check-column id --fields-terminated-by "\t" --hive-delims-replacement "anything"  --null-string "\\\\N" --null-non-string "\\\\N"   --hive-table  public_new_users_user_hold_payment --target-dir $hdfs_base_dir/temp/dealwallet.com/public_new_users_user_hold_payment -- --schema public
