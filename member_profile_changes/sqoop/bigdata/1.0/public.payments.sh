#!/bin/bash
. /home/engineering/member_profile/config.properties
ods_inserted_ts=$(TZ='America/Los_Angeles' date +'%Y-%m-%d %H:%M:%S.%6N-07:00')
echo $ods_inserted_ts
ods_inserted_ts_utc=$(TZ='Etc/UTC' date +'%Y-%m-%d %H:%M:%S.%6N+00:00')
echo $ods_inserted_ts_utc

sqoop job --create payments -- import -m 1  --connect "$db_admin_url" --username postgres --password postgres123 --query "select '$ods_inserted_ts'::text,'$ods_inserted_ts_utc'::text as utc, upo_user_id, upo_payment_group_id, upo_payment_method_id, upo_third_party, upo_payment_email_address, upo_last_modified, source_ip_address::text, source_country, upo_yandex_id from payments where \$CONDITIONS" --incremental append -check-column upo_user_id --fields-terminated-by "\t" --hive-delims-replacement "anything"  --null-string "\\\\N" --null-non-string "\\\\N"   --hive-table payments --target-dir $hdfs_base_dir/temp/dealwallet.com/public_new_payments -- --schema public
