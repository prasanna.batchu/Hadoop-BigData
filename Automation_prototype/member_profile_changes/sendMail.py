#importing the required packages
import smtplib
import sys
import ConfigParser

#Configuration properties reading
config = ConfigParser.ConfigParser()
config.read('config.properties')

gmail_user  = config.get('Properties','gmail_user')
gmail_pwd   = config.get('Properties','gmail_pwd')
To = config.get('Properties','To')

def mailSend(gmail_user, gmail_pwd, To, Subject, Test):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(gmail_user, gmail_pwd)
    BODY = '\r\n'.join(['To: %s' % To,
           'From: %s' % gmail_user,
           'Subject: %s' % Subject,
           '', Test])
    server.sendmail(gmail_user, [To], BODY)
    server.quit()
    print 'email successfully sent to '+To
mailSend(gmail_user, gmail_pwd, To, "logFiles location information", "report generated for the automation logs are located in the following path \n /var/www/html/member/report/ \n The logFiles status report can be updated to the below url \n <serverip>/member/report/report.html \n http://104.250.159.186/member/report/report.html")
