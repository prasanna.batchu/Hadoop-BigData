#!/bin/sh
echo "remove the data in the ods location event_details"
hdfs dfs -rm -r /user/hue/ods/dealwalletcom/event_details/*

echo "remove the data in the DI location"
hdfs dfs -rm -r /user/hue/di/visits/tenant=dealwallet.com/load_id* /user/hue/di/visits/tenant=dealwallet.com/load_type=current/*

echo "removing the data from DW location visits data"
hdfs dfs -rm -r /user/hue/dw/visits/tenant=dealwallet.com/*

echo "moving the data from eventlogs/archieve location to eventlogs location from "
hdfs dfs -mv /user/eventlog/dealwallet.com/archive_201703/DealwalletEventData_2017-03-08* /user/eventlog/dealwallet.com/ ;
echo "data from archieve location to eventlog location successfully moved"

echo "spark-submit dealwallet_events_logs_ods_load.py"
if spark-submit dealwallet_events_logs_ods_load.py;
then echo "eventlog to ods eventdetails python script successfully completed"
else echo "error occured while running dealwallet_events_logs_ods_load.py file"
exit 1
fi

#date -d "2 days ago" +"%Y-%m-%d %H:%M:%S.%s" > /home/engineering/member_profile_changes/config/last_extract_ts/dw/dealwallet_transform_load_dw_visit_dataintegration.config

echo "spark-submit "$1
if spark-submit $1;
then echo "Data processing from ods level to di location level is successfully completed"
else echo "error occured while processing the data from ods to di"
exit 1
fi

echo "spark-submit dealwallet_transform_load_dw_visit.py"
spark-submit dealwallet_transform_load_dw_visit.py
echo "Data processing from DI level to DW location level is successfully completed"
#else echo "error occured while processing the data from DI to DW"
exit 1
fi
