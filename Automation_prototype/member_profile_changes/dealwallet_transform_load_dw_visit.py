#Importing the required packages
import sys
import time
import datetime
import pytz
import os
import socket, struct
import hashlib
from user_agents import parse
from urlparse import urlparse
from urlparse import parse_qs
import IP2Location
import commands
import re
import ConfigParser

from pyspark import SparkContext, SparkConf

from pyspark.storagelevel import StorageLevel

# sys.path.append("/home/hdpdev/bigdata/python/modules/")
# import dealwallet_etl_utils
# from dealwallet_etl_utils import hash_md5, coalesce

config = ConfigParser.RawConfigParser()
config.read('config.properties')

tenant='dealwallet.com'
user=os.environ["USER"]

if user not in ['hdpdev','hdpqa','hdpprod','root','engineering']:
    print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
    sys.exit(1234)
else:
    base_dir = config.get('Properties', 'base_dir')
    hdfs_base_dir = config.get('Properties','hdfs_base_dir')
    impala_server = config.get('Properties','impala_server')

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

#script=sys.argv[0]
script='dealwallet_transform_load_dw_visit'


def getValFromMaxKey(key1, val1,key2, val2):
    if key1>key2:
        return val1
    else:
        return val2

def getIfNoVal(list,index,default_value):
    try:
        if len(list)>=index+1:
            return list[index]
        else:
            return default_value
    except Exception:
        return None

def cleanformat(v):
    try:
        if v is None:
            return ''
        else:
            return unicode(str(v))
    except:
        return 'error'

bkp_dir='backup_id='+str(datetime.datetime.fromtimestamp(time.time())).replace("-","").replace(" ","").replace(":","").replace(".","")

old_bkp_dir='backup_id='+str(datetime.datetime.fromtimestamp(time.time())+datetime.timedelta(days=-4)).replace("-","").replace(" ","").replace(":","").replace(".","")[:8]+'*'

print ""
print ""
print ""
print "Old backup directories:"+old_bkp_dir
print "New backup directory:"+bkp_dir
print ""
print ""
print ""

status, output = commands.getstatusoutput("hdfs dfs -ls -d "+hdfs_base_dir+"/di/visits/tenant="+tenant+"/load_type=current/load_id=*")
output_lines=output.split("\n")

files=[f[f.index("/"):] for f in output_lines]

if "No such file or directory" in files[0]:
    print "No Files to load to dw.visits!!!"
    time_stamp_str=str(datetime.datetime.fromtimestamp(time.time())).replace("-","").replace(" ","").replace(":","").replace(".","")
    no_data_counter=base_dir+'/../data/dw_visits_no_data_counter/dealwalletcom_no_data_'+time_stamp_str
    no_data_counter_file=open(no_data_counter,'w')
    no_data_counter_file.write(str(time_stamp_str))
    no_data_counter_file.close()
    status, output = commands.getstatusoutput("ls -l "+base_dir+"/../data/dw_visits_no_data_counter/dealwalletcom_no_data_*")
    output_lines=output.split("\n")
    counterfiles=[f[f.index("/"):] for f in output_lines]
    if len(counterfiles)>=3:
        print "No data to load for dw.visits in last 3 runs. Please check the HDFS directory "+hdfs_base_dir+"/di/visits/tenant="+tenant+"/load_type=current/. Aborting the job!!!"
        sys.exit(9999)
    print "Skipping the job run!!!"
    sys.exit(0)
else:
    status, output = commands.getstatusoutput("rm "+base_dir+"/../data/dw_visits_no_data_counter/dealwalletcom_no_data_*")

print ""
print ""
print ""
print "Source Data Files from DI Visits:"
print files
print ""
print ""
print ""


conf = (SparkConf().setAppName(script).set("spark.ui.port","0"))
sc = SparkContext(conf = conf)


visit_event_di= sc.textFile(",".join(files)).map(lambda e: e.split("\t"))


#new_event_id=visit_event_step9.map(lambda v: v[4]).keyBy(lambda e:e)

# return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+'/load/visits/'+tenant)
# return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+'/prev_bkp/visits/'+tenant)
# return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+'/dw/visits/tenant='+tenant+'/visits_date_id=19000101')
# return_code=commands.getstatusoutput('hadoop fs -touchz '+hdfs_base_dir+'/dw/visits/tenant='+tenant+'/visits_date_id=19000101/part-001')
return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+'/temp/visits/tenant='+tenant)
return_code=commands.getstatusoutput('hdfs dfs -rm -r '+hdfs_base_dir+'/temp/visits/tenant='+tenant+'/*')
return_code=commands.getstatusoutput('hdfs dfs -rm -r '+hdfs_base_dir+'/load/visits/'+tenant+'/*')
return_code=commands.getstatusoutput('hdfs dfs -rm -r '+hdfs_base_dir+'/prev_bkp/visits/tenant='+tenant+"/"+old_bkp_dir)
add_bkp_dir_script='hdfs dfs -mkdir -p '+hdfs_base_dir+'/prev_bkp/visits/tenant='+tenant+"/"+bkp_dir

try_again=1
return_code=1
while try_again<4 and return_code<>0:
    print "Try No: " +str(try_again)
    print add_bkp_dir_script
    return_code=commands.getstatusoutput(add_bkp_dir_script)
    try_again = try_again+1
    return_code=return_code[0]
if return_code <>0 :
    print "Error adding backup directory!!!"
    sys.exit(9999)


num_records =visit_event_di.count()

print ""
print ""
print ""
print "Number of records to load:"+str(num_records)
print ""
print ""
print ""

key_dates=visit_event_di.map(lambda v:(v[6],1)).keyBy(lambda e: e[0]).reduceByKey(lambda v1,v2:getValFromMaxKey(v1[1], v1, v2[1],v2)).collectAsMap()



dw_inserted_ts=str(datetime.datetime.fromtimestamp(time.time()))
dw_inserted_ts_utc = str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)))


for d in key_dates.keys():
    return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+'/dw/visits/tenant='+tenant+'/visits_date_id='+str(d))
    old_visit_match_dates=sc.textFile(hdfs_base_dir+"/dw/visits/tenant="+tenant+"/visits_date_id="+str(d)+"/").map(lambda e: e.split("\t"))
    new_visit_match_dates=visit_event_di.filter(lambda x:x[6]==d)
    visit_load = old_visit_match_dates.union(new_visit_match_dates).keyBy(lambda v:v[14]).reduceByKey(lambda v1,v2:getValFromMaxKey(v1[1], v1, v2[1],v2)).map(lambda (k,v):v)
    visit_load_final=visit_load.filter(lambda x:x[6]==d).map(lambda v:(cleanformat(v[0])+unicode("\t")+cleanformat(v[1])+unicode("\t")+cleanformat(v[2])+unicode("\t")+cleanformat(v[3])+unicode("\t")+cleanformat(v[4])+unicode("\t")+cleanformat(v[5])+unicode("\t")+cleanformat(v[6])+unicode("\t")+cleanformat(v[7])+unicode("\t")+cleanformat(v[8])+unicode("\t")+cleanformat(v[9])+unicode("\t")+cleanformat(v[10])+unicode("\t")+cleanformat(v[11])+unicode("\t")+cleanformat(v[12])+unicode("\t")+cleanformat(v[13])+unicode("\t")+cleanformat(v[14])+unicode("\t")+cleanformat(v[15])+unicode("\t")+cleanformat(v[16])+unicode("\t")+cleanformat(v[17])+unicode("\t")+cleanformat(v[18])+unicode("\t")+cleanformat(v[19])+unicode("\t")+cleanformat(v[20])+unicode("\t")+cleanformat(v[21])+unicode("\t")+cleanformat(v[22])+unicode("\t")+cleanformat(v[23])+unicode("\t")+cleanformat(v[24])+unicode("\t")+cleanformat(v[25])+unicode("\t")+cleanformat(v[26])+unicode("\t")+cleanformat(v[27])+unicode("\t")+cleanformat(v[28])+unicode("\t")+cleanformat(v[29])+unicode("\t")+cleanformat(v[30])+unicode("\t")+cleanformat(v[31])+unicode("\t")+cleanformat(v[32])+unicode("\t")+cleanformat(v[33])+unicode("\t")+cleanformat(v[34])+unicode("\t")+cleanformat(v[35])+unicode("\t")+cleanformat(v[36])+unicode("\t")+cleanformat(v[37])+unicode("\t")+cleanformat(v[38])+unicode("\t")+cleanformat(v[39])+unicode("\t")+cleanformat(v[40])+unicode("\t")+cleanformat(v[41])+unicode("\t")+cleanformat(v[42])+unicode("\t")+cleanformat(v[43])+unicode("\t")+cleanformat(v[44])+unicode("\t")+cleanformat(v[45])+unicode("\t")+cleanformat(v[46])+unicode("\t")+cleanformat(v[47])+unicode("\t")+cleanformat(v[48])+unicode("\t")+cleanformat(v[49])+unicode("\t")+cleanformat(v[50])+unicode("\t")+cleanformat(v[51])+unicode("\t")+cleanformat(v[52])+unicode("\t")+cleanformat(v[53])+unicode("\t")+cleanformat(v[54])+unicode("\t")+cleanformat(v[55])+unicode("\t")+cleanformat(v[56])+unicode("\t")+cleanformat(v[57])+unicode("\t")+cleanformat(v[58])+unicode("\t")+cleanformat(v[59])+unicode("\t")+cleanformat(v[60])+unicode("\t")+cleanformat(v[61])+unicode("\t")+cleanformat(v[62])+unicode("\t")+cleanformat(v[63])+unicode("\t")+cleanformat(v[64])+unicode("\t")+cleanformat(v[65])+unicode("\t")+cleanformat(v[66])+unicode("\t")+cleanformat(v[67])+unicode("\t")+cleanformat(v[68])+unicode("\t")+cleanformat(v[69])+unicode("\t")+cleanformat(v[70])+unicode("\t")+cleanformat(v[71]) +unicode("\t")+cleanformat(v[72])+unicode("\t")+cleanformat(v[73])+unicode("\t")+cleanformat(getIfNoVal(v,74,''))+unicode("\t")+cleanformat(getIfNoVal(v,75,''))+unicode("\t")+cleanformat(getIfNoVal(v,76,''))+unicode("\t")+cleanformat(getIfNoVal(v,77,''))+unicode("\t")+cleanformat(getIfNoVal(v,78,''))+unicode("\t")+cleanformat(getIfNoVal(v,79,''))+unicode("\t")+cleanformat(getIfNoVal(v,80,''))+unicode("\t")+cleanformat(getIfNoVal(v,81,''))+unicode("\t")+cleanformat(getIfNoVal(v,82,''))+unicode("\t")+cleanformat(getIfNoVal(v,83,''))))
    # visit_load_final.saveAsTextFile(hdfs_base_dir+"/temp/visits/tenant="+tenant+"/visits_date_id="+str(d))
    # write_dw_visits=sc.textFile(hdfs_base_dir+"/temp/visits/tenant="+tenant+"/visits_date_id="+str(d)+"/part*")
    visit_load_final.coalesce(1, shuffle = True).saveAsTextFile(hdfs_base_dir+"/load/visits/"+tenant+"/visits_date_id="+str(d))


for d in key_dates.keys():
    hdfs_cmd ='hdfs dfs -mv '+hdfs_base_dir+'/dw/visits/tenant='+tenant+'/visits_date_id='+str(d)+' '+hdfs_base_dir+'/prev_bkp/visits/tenant='+tenant+"/"+bkp_dir+'/'
    print hdfs_cmd
    return_code=commands.getstatusoutput(hdfs_cmd)
    if return_code[0] <>0:
        print "Error in moving dw files to backup!!!"
    hdfs_cmd='hdfs dfs -mv '+hdfs_base_dir+'/load/visits/'+tenant+'/visits_date_id='+str(d)+' '+hdfs_base_dir+'/dw/visits/tenant='+tenant+'/'
    print hdfs_cmd
    return_code=commands.getstatusoutput(hdfs_cmd)
    if return_code[0] <>0 :
        print "Error in moving data files to dw!!!"
    add_partition_script="impala-shell -i "+impala_server+' -q "alter table dw.visits add if not exists partition (tenant=\''+tenant+'\',visits_date_id='+str(d)+')"'
    try_again=1
    return_code=1
    while try_again<4 and return_code<>0:
        print "Try No: " +str(try_again)
        print add_partition_script
        return_code=commands.getstatusoutput(add_partition_script)
        try_again = try_again+1
        return_code=return_code[0]
    if return_code <>0 :
        print "Error in adding partition to impala table!!!"
        sys.exit(9999)

#Move the processed directory to archive
###############################

cmd_file=base_dir+'/exe/remove_processes_dealwalletcom_visits_di_hdfs_files.sh'
cmd="hdfs dfs -rm -r "+" ".join(files)
archive_cmd_file=open(cmd_file,'w')
archive_cmd_file.write(str(cmd))
archive_cmd_file.close()
print cmd
print ""
print ""
cmd="sh "+cmd_file
try_again=1
return_code=1
while try_again<4 and return_code<>0:
    print "Try No: " +str(try_again)
    print cmd
    return_code=commands.getstatusoutput(cmd)
    try_again = try_again+1
    return_code=return_code[0]
if return_code <>0 :
    print "Error in removing processed visits hdfs files from di staging layer!!!"
    sys.exit(9999)

print ""
print ""
print ""
print "Removed the processed hdfs files..."
print ""
print ""
print ""
print ""

print "Number of records loaded:"
print str(num_records)
print "Job completed successfully"
print ""
print ""
print ""
print ""
print ""
sc.stop()

