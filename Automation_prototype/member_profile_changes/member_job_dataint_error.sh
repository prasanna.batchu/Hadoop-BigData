echo "job2_auto_test.sh file is successfully started"
start_time=$(date +%s)
./job2_auto_test.sh $2 > memberjob.log
echo "job2_auto_test.sh file is successfully completed with generating a log file at location /home/engineering/member_profile_changes/memberjob.log"
finish_time=$(date +%s)
echo "Time duration: $((finish_time - start_time)) secs."
echo min=$(( $((finish_time - start_time)) /60 )) min.

echo "spark-submit successfully started"
start_time=$(date +%s)
spark-submit --master yarn --num-executors 4 --executor-memory 20g --driver-memory 1g --executor-cores 20 --conf spark.scheduler.mode=FAIR --conf spark.yarn.queue=low_priority_jobs --conf spark.executor.extraJavaOptions=-XX:+UseG1GC --conf spark.sql.tungsten.enabled=true --class MemberProfileTest --conf spark.yarn.jar=local:/opt/cloudera/parcels/CDH/lib/spark/lib/spark-assembly-1.6.0-cdh5.9.0-hadoop2.6.0-cdh5.9.0.jar $1 > sparkjob.log
finish_time=$(date +%s)
echo "Time duration: $((finish_time - start_time)) secs."
echo min=$(( $((finish_time - start_time)) /60 )) min.

echo "job3_auto_test.sh file is successfully started"
start_time=$(date +%s)
./job3_auto_test.sh >> memberjob.log
echo "job3_auto_test.sh file is successfully completed with generating a log file at location /home/engineering/member_profile_changes/memberjob.log"
finish_time=$(date +%s)
echo "Time duration: $((finish_time - start_time)) secs."
echo min=$(( $((finish_time - start_time)) /60 )) min.

#cat memberjob.log |pastebinit &> linkfile.log

#python sendmailtesting.py

#python hipchatlinksend.py


