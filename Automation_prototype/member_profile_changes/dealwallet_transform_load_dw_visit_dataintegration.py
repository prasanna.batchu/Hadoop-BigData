#Importing the required packages
import sys
import time
import datetime
import pytz
import os
import socket, struct
import hashlib
from user_agents import parse
from urlparse import urlparse
from urlparse import parse_qs
import IP2Location
import commands
import re
import ConfigParser

from pyspark import SparkContext, SparkConf

from pyspark.storagelevel import StorageLevel


# sys.path.append("/home/hdpdev/bigdata/python/modules/")
# import dealwallet_etl_utils
# from dealwallet_etl_utils import hash_md5, coalesce

config = ConfigParser.RawConfigParser()
config.read('config.properties')

tenant='dealwallet.com'
user=os.environ["USER"]

if user not in ['hdpdev','hdpqa','hdpprod','root','engineering']:
    print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
    sys.exit(1234)
else:
    base_dir = config.get('Properties', 'base_dir')
    hdfs_base_dir = config.get('Properties','hdfs_base_dir')
    impala_server = config.get('Properties','impala_server')

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

#script=sys.argv[0]
script='dealwallet_transform_load_dw_visit_dataintegration'
last_extract_value_file_name=base_dir+'/config/last_extract_ts/dw/'+script+'.config'
new_last_extract_value=str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)))
is_signup = lambda x: x is not None and 1 or 0

is_login = lambda x: x is not None and 1 or 0




def coalesce(*arg):
    for el in arg:
        if el is not None and el <>'None' and el <>'NULL' and el <> 'null'  and el <> 'Null' and el <> '\\N':
            return el
    return None

def getNonEmpty(*arg):
    for el in arg:
        if el is not None and el <>'None' and el <>'NULL' and el <> 'null'  and el <> 'Null' and el <> '\\\\N' and el <> '\\N' and el.strip()<>'':
            return el
    return None

def isTrue(*arg):
    for el in arg:
        if el is not None and el  :
            return True
    return False




def decodeExpr(*arg):
    arg_count=len(arg)
    for x in range(0,arg_count-1,2):
        if arg[x] is not None and arg[x]   :
            return arg[x+1]
    return None

def hash_md5(s):
    if s is  None or s =='':
        return -1
    return hashlib.md5(s).hexdigest()


def url_string_cleanup (str):
    if str is None:
        return None
    str=str.replace('25252525252525252520',' ').replace('252525252520',' ').replace('%252520',' ').replace('dealwallet20TM','dealwalletTM').replace('dealwallet25TM','dealwalletTM').replace('%2520',' ').replace('%20',' ').replace('%28',' ').replace('%29',' ').replace('%c2',' ').replace('%ae',' ').replace(':',' ').replace(';',' ').replace('"',' ').replace('-',' ').replace('+',' ').replace('*',' ').replace('.',' ').replace('/',' ').replace('=',' ').replace('%',' ').replace(')',' ').replace('(',' ').replace(',',' ').replace('>',' ').replace('<',' ').replace('?',' ').replace('[',' ').replace(']',' ').replace('#','').replace(' ','')
    if str.lower().find('dealwallet')>-1 and str.lower().find('tm',str.lower().find('dealwallet'))>-1:
        str='dealwallettm'
    str=str.lower().strip()
    return str

def getDomain(url):
    if url is None or url =='' :
        return None
    url=url.encode('utf-8','ignore').decode('utf-8')
    url="".join([ch for ch in url if ord(ch)<= 128])
    parsed_uri = urlparse( url.strip() )
    domain = '{uri.netloc}'.format(uri=parsed_uri)
    return domain

def isPatternInString(str, pattern):
    if str is None or pattern is None:
        return None
    if pattern in str:
        return True
    return False

def is_referrer_id_in_string(str):
    return isPatternInString(str, 'referrerid=') or isPatternInString(str, 'ref_id=') or isPatternInString(str, 'dealwallet.com/r/')


def extract_url_param( url, param, delim='&' ):
    if url is None or url =='':
        return None
    if param is None or param=='':
        return None
    try:
        start = url.index( param ) + len( param )
        try:
            end = url.index( delim, start )
        except ValueError:
            end = len(url)
        return url[start:end]
    except ValueError:
        return None

def extract_referrer_id_param(url, delim='&'):
    result = extract_url_param(url, 'referrerid=', delim)
    if result is None:
        return extract_url_param(url, 'ref_id=', delim)
    return result


def format_ts(ts):
    if len(ts)==19 :
        return ts+'.000000'
    elif len(ts)>19 :
        return (ts+'000000')[:26]
    else:
        return '1900-01-01 00:00:00.000000'

def getValFromMaxKey(key1, val1,key2, val2):
    if key1>key2:
        return val1
    else:
        return val2

def dateidAdd(dateid,number_of_days):
    new_date=datetime.datetime.strptime(str(dateid),'%Y%m%d') +datetime.timedelta(days=number_of_days)
    return new_date.strftime('%Y%m%d')

def is_IPbot(ip,visit_date_id, botIPDB):
    if ip.startswith('10.'):
        return True
    for bot in botIPDB.value:
        if ip == bot[1] and dateidAdd(visit_date_id,-2)<=bot[0] and dateidAdd(visit_date_id,2)>=bot[0]:
            return True
    return False


def is_UAbot(UAParser_flag, UA, ip, request_url, bc_bot_agents, ip_to_exclude, url_to_exclude,  request_url_query=''):
    if UAParser_flag == True:
        return True
    if ip in ip_to_exclude.value:
        return True
    if request_url.replace(' ','') in url_to_exclude.value:
        return True
    if coalesce(request_url,'').replace(' ','')+coalesce(request_url_query,'').replace(' ','') =='http://www.dealwallet.com/index.htmlogout=true' or coalesce(request_url,'').replace(' ','')+coalesce(request_url_query,'').replace(' ','') =='https://www.dealwallet.com/index.htmlogout=true':
        return True
    for botua in bc_bot_agents.value:
        if botua in UA:
            return True
    return False


def is_URLbot(UAbot_flag,request_url_stem,request_url_query,user_id,visits_date_id):
    print request_url_stem+'?'+request_url_query
    if request_url_stem+'?'+request_url_query =='http://www.dealwallet.com/auth/getLogonForm.do?pageName=/common_templates/login.vm' and visits_date_id >=20161101 and visits_date_id <=20161117 and user_id==0 :
        return True
    return UAbot_flag



def ip_location(ip):
    IP2LocObj = IP2Location.IP2Location()
    IP2LocObj.open(base_dir+"/data/IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE-ISP-DOMAIN.BIN");
    try:
        rec = IP2LocObj.get_all(ip);
        results = [ip]+[ coalesce(rec.country_short,'N/A').replace(' ','')+"_"+coalesce(rec.country_long,'N/A').replace(' ','')+"_"+coalesce(rec.region,'N/A').replace(' ','')+"_"+coalesce(rec.city,'N/A').replace(' ','')+"_"+coalesce(rec.zipcode,'N/A').replace(' ',''),    coalesce(rec.country_short,'N/A'),coalesce(rec.country_long,'N/A'),coalesce(rec.region,'N/A'),coalesce(rec.city,'N/A'),coalesce(rec.zipcode,'N/A')]
    except:
        results = [ip]+[ 'N/A'+"_"+'N/A'+"_"+'N/A'+"_"+'N/A'+"_"+'N/A',   'N/A','N/A','N/A','N/A','N/A']
    return results



def getOSBrowser(ua, sourcename = None):
    try:
        if sourcename == 'iOS-Tablet' or sourcename == 'iOS-STablet':
            return [ua]+['iPad_iOS_N/A_MobileSafari_N/A_False_False_True_False','iPad','iOS','N/A','Mobile Safari','N/A','False','False','True','False']
        elif sourcename == 'iOS' or sourcename == 'iOS-Phone' or sourcename == 'iOS-SPhone':
            return [ua]+['iPhone_iOS_N/A_MobileSafari_N/A_False_True_False_False','iPhone','iOS','N/A','Mobile Safari','N/A','False','True','False','False']
        if sourcename == 'Android-Tablet' or sourcename == 'Android-STablet':
            return [ua]+['Android-Tablet_Android_N/A_Android_N/A_False_False_True_False','Android-Tablet','Android','N/A','Android','N/A','False','False','True','False']
        elif sourcename == 'Android-Phone' or sourcename == 'Android-SPhone':
            return [ua]+['Android-Phone_Android_N/A_Android_N/A_False_True_False_False','Android-Phone','Android','N/A','Android','N/A','False','True','False','False']
        if isPatternInString(ua,'dealwallet'):
            if isPatternInString(ua,'CFNetwork'):
                if sourcename == 'iOS-Tablet':
                    return [ua]+['iPad_iOS_N/A_MobileSafari_N/A_False_False_True_False','iPad','iOS','N/A','Mobile Safari','N/A','False','False','True','False']
                elif sourcename == 'iOS' or sourcename == 'iOS-Phone':
                    return [ua]+['iPhone_iOS_N/A_MobileSafari_N/A_False_True_False_False','iPhone','iOS','N/A','Mobile Safari','N/A','False','True','False','False']
                else:
                    return [ua]+['iPhone_iOS_N/A_MobileSafari_N/A_False_True_False_False','iPhone','iOS','N/A','Mobile Safari','N/A','False','True','False','False']
            elif re.search(r'(iPad); ([0-9]*)',ua,re.M|re.I)!=None:
                if re.search(r'(iPad); ([0-9]*)',ua,re.M|re.I).group(1) == 'iPad':
                    return [ua]+['iPad_iOS_'+re.search(r'(iPad); ([0-9]*)',ua,re.M|re.I).group(2)+'_MobileSafari_N/A_False_False_True_False','iPad','iOS',re.search(r'(iPad); ([0-9]*)',ua,re.M|re.I).group(2),'Mobile Safari','N/A','False','False','True','False']
            elif re.search(r'(iPhone); ([0-9]*)',ua,re.M|re.I)!=None:
                if re.search(r'(iPhone); ([0-9]*)',ua,re.M|re.I).group(1) == 'iPhone':
                    return [ua]+['iPhone_iOS_'+re.search(r'(iPhone); ([0-9]*)',ua,re.M|re.I).group(2)+'_MobileSafari_N/A_False_True_False_False','iPhone','iOS',re.search(r'(iPhone); ([0-9]*)',ua,re.M|re.I).group(2),'Mobile Safari','N/A','False','True','False','False']
            elif re.search(r'(Android Tablet); ([0-9]*)',ua,re.M|re.I)!=None:
                if re.search(r'(Android Tablet); ([0-9]*)',ua,re.M|re.I).group(1) == 'Android Tablet':
                    return [ua]+['Android-Tablet_Android_'+re.search(r'(Android Tablet); ([0-9]*)',ua,re.M|re.I).group(2)+'_Android_N/A_False_False_True_False','Android-Tablet','Android',re.search(r'(Android Tablet); ([0-9]*)',ua,re.M|re.I).group(2),'Android','N/A','False','False','True','False']
            elif re.search(r'(Android Phone); ([0-9]*)',ua,re.M|re.I)!=None:
                if re.search(r'(Android Phone); ([0-9]*)',ua,re.M|re.I).group(1) == 'Android Phone':
                    return [ua]+['Android-Phone_Android_'+re.search(r'(Android Phone); ([0-9]*)',ua,re.M|re.I).group(2)+'_Android_N/A_False_True_False_False','Android-Phone','Android',re.search(r'(Android Phone); ([0-9]*)',ua,re.M|re.I).group(2),'Android','N/A','False','True','False','False']
            elif sourcename == 'iOS-Tablet':
                return [ua]+['iPad_iOS_N/A_MobileSafari_N/A_False_False_True_False','iPad','iOS','N/A','Mobile Safari','N/A','False','False','True','False']
            elif sourcename == 'iOS' or sourcename == 'iOS-Phone':
                return [ua]+['iPhone_iOS_N/A_MobileSafari_N/A_False_True_False_False','iPhone','iOS','N/A','Mobile Safari','N/A','False','True','False','False']
            else:
                return [ua]+['N/A_N/A_N/A_N/A_N/A_N/A_N/A_N/A_N/A','N/A','N/A','N/A','N/A','N/A','N/A','N/A','N/A','N/A']
        if isPatternInString(ua,'dealwallet') and isPatternInString(ua,'iPod'):
            return [ua]+['iPod_iOS_N/A_MobileSafari_N/A_False_True_False_False','iPod','iOS','N/A','Mobile Safari','N/A','False','True','False','False']
        if isPatternInString(ua.lower(),'okhttp/') and len(ua.strip())<20:
            if sourcename == 'Android-Tablet':
                return [ua]+['Android-Tablet_Android_N/A_Android_N/A_False_False_True_False','Android-Tablet','Android','N/A','Android','N/A','False','False','True','False']
            if sourcename == 'Android-Phone':
                return [ua]+['Android-Phone_Android_N/A_Android_N/A_False_True_False_False','Android-Phone','Android','N/A','Android','N/A','False','True','False','False']
        user_agent = parse(ua)
        device_family=str(user_agent.device.family)
        os_family=str(user_agent.os.family)
        os_version_string=str(user_agent.os.version_string)
        browser_family=str(user_agent.browser.family)
        browser_version_string=str(user_agent.browser.version_string)
        is_bot=str(user_agent.is_bot)
        is_mobile=str(user_agent.is_mobile)
        is_tablet=str(user_agent.is_tablet)
        is_pc=str(user_agent.is_pc)
        if str(user_agent.os.family).startswith("Windows"):
            os_family="Windows"
            os_version_string=user_agent.os.family.replace("Windows","").replace(" ","")
        results = [ua]+[coalesce(device_family,'N/A').replace(' ','')+"_"+coalesce(os_family,'N/A').replace(' ','')+"_"+  coalesce(os_version_string,'N/A').replace(' ','')+"_"+ coalesce(browser_family,'N/A').replace(' ','')+"_"+   coalesce(browser_version_string,'N/A').replace(' ','')+"_"+   coalesce(is_bot,'N/A').replace(' ','')+"_"+   coalesce(is_mobile ,'N/A').replace(' ','')+"_"+  coalesce(is_tablet ,'N/A').replace(' ','')+"_"+  coalesce( is_pc,'N/A').replace(' ','') ] + [coalesce(device_family,'N/A'), coalesce(os_family,'N/A'), coalesce(os_version_string,'N/A'), coalesce(browser_family,'N/A'),  coalesce(browser_version_string,'N/A'),  coalesce(is_bot,'N/A'),   coalesce(is_mobile ,'N/A'), coalesce(is_tablet ,'N/A'), coalesce( is_pc ,'N/A')]
    except Exception ,e:
        print str(e)
        results = [ua]+['N/A_N/A_N/A_N/A_N/A_N/A_N/A_N/A_N/A','N/A','N/A','N/A','N/A','N/A','N/A','N/A','N/A','N/A']
    return results



def getVisitApplication(sourcename,ua, eeid):
    if sourcename in ('iOS','iOS-Phone','iOS-SPhone') or (isPatternInString(ua,'dealwallet') and isPatternInString(ua,'CFNetwork')):
        return ['App_iPhone',	'App',	'iPhone']
    elif sourcename =='iOS-Tablet' or sourcename =='iOS-STablet':
        return ['App_iPad',	'App',	'iPad']
    elif sourcename =='Android-Phone' or sourcename =='Android-SPhone':
        return ['App_Android_Phone',	'App',	'Android Phone']
    elif sourcename =='Android-Tablet' or sourcename =='Android-STablet':
        return ['App_Android_Tablet',	'App',	'Android Tablet']
    elif sourcename =='toolbar':
        return ['Toolbar_Toolbar',	'Toolbar',	'Toolbar']
    elif eeid in ('23509','26141' ,'26142' ,'26118','26117'):
        return ['Toolbar_Toolbar_Toolbar','Toolbar',	'Toolbar',	'Toolbar']
    else:
        return ['Website_Website','Website','Website']

def isBranded(kw, branded_kw):
    if kw is None:
        return None
    if url_string_cleanup(kw) in branded_kw.value :
        return "Branded"
    for v in branded_kw.value:
        if  url_string_cleanup(kw).startswith(url_string_cleanup(v)):
            return "Branded"
    return "Unbranded"

def getPaidMarketingPartner(src,acct, pma):
    if src is None and acct is None:
        return None
    if src=='criteo':
        return 'criteo'
    for i in range (len(pma.value)):
        if pma.value[i][0].lower() == coalesce(acct,'').lower():
            return   pma.value[i][1]
    return None



def getSocialMediaChannel(src,acct,url, smp):
    if src is None:
        return None
    for i in range (len(smp.value)-1):
        if smp.value[i][0]=='src' and src ==smp.value[i][1]:
            return smp.value[i][2]
        elif smp.value[i][0]=='acct' and acct ==smp.value[i][1]:
            return smp.value[i][2]
        elif smp.value[i][0]=='pattern_url' and smp.value[i][1] in url:
            return smp.value[i][2]
    return None

def getSEONetwork(url, sup):
    if url is None:
        return None
    for i in range (len(sup.value)):
        if sup.value[i][0] in url:
            return   sup.value[i][1]
    return None

def getLeadgen(utype, leadgen):
    if utype is None:
        return None
    for i in range (len(leadgen.value)-1):
        if leadgen.value[i][0].lower() ==str(utype).lower():
            return   leadgen.value[i][1]
    return None

def getVisitTrafficSource (user_id, QAMembers, is_referral, eeid, email_eeid, sourcename, sem_network, is_gglnum, is_product,kw_type, pm_partner, social_media, leadgen, seo_network , search_type, is_weblinks,ua, src,acct):
    if user_id in QAMembers.value:
        return ['NotApplicable_QAMembers_QAMembers','Not Applicable',	'QA Members',	'QA Members']
    elif is_referral:
        return ['ReferralLink_ReferralLink_ReferralLink','Referral Link',	'Referral Link',	'Referral Link']
    elif eeid in ('23509','26141' ,'26142' ,'26118','26117'):
        return ['Toolbar_Toolbar_Toolbar','Toolbar',	'Toolbar',	'Toolbar']
    elif eeid in email_eeid.value:
        return ['Email_Email_Email','Email',	'Email',	'Email']
    elif sourcename in ('iOS','iOS-Phone') or (isPatternInString(ua,'dealwallet') and isPatternInString(ua,'CFNetwork')):
        return ['dealwalletMobileApp_dealwalletMobileApp_iPhoneApp','dealwallet Mobile App',	'dealwallet Mobile App',	'iPhone App']
    elif sourcename =='iOS-Tablet':
        return ['dealwalletMobileApp_dealwalletMobileApp_iPadApp','dealwallet Mobile App',	'dealwallet Mobile App',	'iPad App']
    elif sourcename =='Android-Phone':
        return ['dealwalletMobileApp_dealwalletMobileApp_AndroidPhoneApp','dealwallet Mobile App',	'dealwallet Mobile App',	'Android Phone App']
    elif sourcename =='Android-Tablet':
        return ['dealwalletMobileApp_dealwalletMobileApp_AndroidTabletApp','dealwallet Mobile App',	'dealwallet Mobile App',	'Android Tablet App']
    elif sourcename =='toolbar':
        return ['Toolbar_Toolbar_Toolbar',	'Toolbar',	'Toolbar','Toolbar']
    elif coalesce(src,'').lower()=="spr":
        return ['Partner_Shopular_Shopular','Partner','Shopular','Shopular']
    elif coalesce(src,'').lower()=="fwm":
        return ['Partner_FatWallet_FatWallet','Partner','FatWallet','FatWallet']
    elif sem_network=='Google':
        if is_gglnum:
            if acct=='core':
                return ['SEM_Content_Google','SEM','Content','Google']
            elif is_product:
                return ['SEM_Product_Google','SEM','Product','Google']
            elif kw_type=="Branded":
                return ['SEM_Branded_Google','SEM','Branded','Google']
            else:
                return ['SEM_Unbranded_Google','SEM','Unbranded','Google']
        else:
            return ['WebLinks_WebLinks_WebLinks','Web Links','Web Links','Web Links']
    elif sem_network=='MSN':
        if acct=='core':
            return ['SEM_Content_MSN','SEM','Content','MSN']
        elif is_product:
            return ['SEM_Product_MSN','SEM','Product','MSN']
        elif kw_type=="Branded":
            return ['SEM_Branded_MSN','SEM','Branded','MSN']
        else:
            return ['SEM_Unbranded_MSN','SEM','Unbranded','MSN']
    elif sem_network=='Yahoo':
        if acct=='core':
            return['SEM_Content_Yahoo','SEM','Content','Yahoo']
        elif is_product:
            return ['SEM_Product_Yahoo','SEM','Product','Yahoo']
        elif kw_type=="Branded":
            return ['SEM_Branded_Yahoo','SEM','Branded','Yahoo']
        else:
            return ['SEM_Unbranded_Yahoo','SEM','Unbranded','Yahoo']
    elif coalesce(src,'').lower()=="video":
        return ['Video_Video_'+coalesce(acct,'N/A').replace(" ",""),'Video','Video',coalesce(acct,'N/A')]
    elif pm_partner is not None :
        return ['PaidMarketing_PaidMarketing_'+pm_partner.replace(" ",""),'Paid Marketing',	'Paid Marketing',	pm_partner]
    elif social_media is not None:
        return ['Others_SocialMedia_'+social_media.replace(" ",""),'Others',	'Social Media',	social_media]
    elif leadgen is not None:
        return ['LeadGen_LeadGen_'+leadgen.replace(" ",""),'Lead Gen',	'Lead Gen',	leadgen]
    elif seo_network is not None:
        if search_type=="Branded":
            return ['SEO_Branded_'+seo_network.replace(" ",""),'SEO','Branded',seo_network]
        else:
            return ['SEO_Unbranded_'+seo_network.replace(" ",""),'SEO','Unbranded',seo_network]
    elif is_weblinks :
        return ['WebLinks_WebLinks_WebLinks','Web Links','Web Links','Web Links']
    else:
        return ['Direct_Direct_Direct','Direct','Direct','Direct']



def getVisitTrafficSourceFromURLParams(utm_medium,utm_campaign,utm_source,utm_content,request_url,referring_url,kw,branded_kw,param1='referral=',param2='referrerid=',param3='dealwallet.com/r/',param4='ref_id='):
    if utm_medium=='email':
        return ['Email_Email_Email','Email','Email','Email']
    if utm_medium=='partner':
        return ['Partner_partner_'+utm_content,'Partner','Partner',utm_content]
    if param1 in request_url or param2 in request_url or param1 in referring_url or param2 in referring_url or param3 in request_url or param4 in request_url or param3 in referring_url or param4 in referring_url:
        return ['Referral Link_Referral Link_Referral Link','Referral Link','Referral Link','Referral Link']
    if utm_medium=='video':
        return ['Video_Video_'+utm_source,'Video','Video',utm_source]
    if utm_medium=='paid' or utm_medium=='paidmarketing':
        return ['Paid Marketing_Paid Marketing_'+utm_content,'Paid Marketing','Paid Marketing',utm_content]
    if utm_medium=='social':
        return ['Social_Social_'+utm_content,'Social','Social',utm_content]
    if utm_medium=='sem':
        if utm_campaign=='content':
            return ['SEM_Content_'+utm_source,'SEM','Content',utm_source]
        elif utm_campaign=='product':
            return ['SEM_Product_'+utm_source,'SEM','Product',utm_source]
        else:
            tst=isBranded(kw,branded_kw)
            return ['SEM_'+str(tst)+'_'+utm_source,'SEM',tst,utm_source]
    else:
        return ['','','','']



def getDevicePlatform(device_family,os_str, is_mobile, is_tablet):
    if is_mobile=='True' or device_family=='iPhone':
        return 'Smartphone'
    elif is_tablet=='True' or device_family=='iPad':
        return 'Tablet'
    elif os_str.startswith('iOS'):
        if is_tablet==True:
            return 'Tablet'
        else:
            return 'Smartphone'
    elif os_str.startswith('Android'):
        if is_tablet==True:
            return 'Tablet'
        else:
            return 'Smartphone'
    else:
        return 'Desktop'

def cleanformat(v):
    try:
        if v is None:
            return ''
        else:
            return unicode(str(v))
    except:
        return 'error'


def isNoneStr_to_blank(obj, index):
    if obj is None :
        return ''
    return obj[index]


def listToString(list,delimiter="\t"):
    if list is None:
        return None
    row=''
    for i in range(len(list)):
        row=row+coalesce(unicode(list[i]),'')+delimiter
    return row

def isAllURLParamsPresent(request_url_query,referrer_url_query):
    if (request_url_query is None or request_url_query=='') and (referrer_url_query is None or referrer_url_query==''):
        return False
    request_url_query_params=request_url_query.split('&')
    keys=[]
    for param in request_url_query_params:
        key=param.split('=')[0]
        keys.append(key)
    returnVal=all(x in keys for x in ['utm_medium', 'utm_campaign', 'utm_source', 'utm_content', 'utm_term', 'utm_size', 'utm_pub', 'utm_ebs'])
    if returnVal:
        return returnVal
    else:
        keys=[]
        referrer_url_query_parms=referrer_url_query.split('&')
        for param in referrer_url_query_parms:
            key=param.split('=')[0]
            keys.append(key)
        returnVal=all(x in keys for x in ['utm_medium', 'utm_campaign', 'utm_source', 'utm_content', 'utm_term', 'utm_size', 'utm_pub', 'utm_ebs'])
        return returnVal

conf = (SparkConf().setAppName(script).set("spark.ui.port","0"))
sc = SparkContext(conf = conf)

return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+'/load/di/visits/tenant='+tenant)
return_code=commands.getstatusoutput('hdfs dfs -rm -r '+hdfs_base_dir+'/load/di/visits/tenant='+tenant+"/*")

etl_branded_kw=sc.textFile(hdfs_base_dir+"/ods/etl/branded_keywords/*")
clean_etl_branded_kw=etl_branded_kw.map(lambda x:url_string_cleanup(x))
branded_kw=sc.broadcast(clean_etl_branded_kw.map( lambda x: (x,1)).collectAsMap())

qa_user_list=sc.textFile(hdfs_base_dir+"/ods/dealwalletcom/public_new_users_users/*").coalesce(100).map(lambda u: u.split('\t')).filter(lambda u:"dealwalletqauser" in u[3] or "dealwalletliveuser" in u[3]).map(lambda u:(u[2],u[3]))
QAMembers=sc.broadcast(qa_user_list.map(lambda u:u[0]).map( lambda x: (x,1)).collectAsMap())


external_entry_category=sc.textFile(hdfs_base_dir+"/ods/dealwalletcom/webanalytics_dealwallet_external_entry_category/*").map(lambda u: u.split('\t')).coalesce(20)
external_entry=sc.textFile(hdfs_base_dir+"/ods/dealwalletcom/webanalytics_dealwallet_external_entry/*").map(lambda u: u.split('\t')).coalesce(20)

email_category=external_entry_category.filter(lambda x:x[4]=='1').map(lambda x:x[2]).collect()
email_eeid_list=external_entry.filter(lambda x:x[4] in email_category).map(lambda x:x[2])
email_eeid=sc.broadcast(email_eeid_list.map( lambda x: (x,1)).collectAsMap())


etl_paid_marketing_accounts=sc.textFile(hdfs_base_dir+"/ods/etl/paid_marketing_accounts/*").map(lambda u: u.split('\t'))
paid_marketing_accounts=sc.broadcast(etl_paid_marketing_accounts.collect())


etl_social_media_param=sc.textFile(hdfs_base_dir+"/ods/etl/social_media_param/*").map(lambda u: u.split('\t'))
social_media_param=sc.broadcast(etl_social_media_param.collect())

etl_seo_url_pattern=sc.textFile(hdfs_base_dir+"/ods/etl/seo_url_pattern/*").map(lambda u: u.split('\t'))
seo_url_pattern=sc.broadcast(etl_seo_url_pattern.collect())


etl_leadgen_utype=sc.textFile(hdfs_base_dir+"/ods/etl/leadgen_utype/*").map(lambda u: u.split('\t'))
leadgen_utype=sc.broadcast(etl_leadgen_utype.collect())


bot_ip=sc.textFile(hdfs_base_dir+"/ods/etl/bot_ip/*").map(lambda u: u.split('\t'))
bot_agents=sc.textFile(hdfs_base_dir+"/ods/etl/bot_agents/*")


bc_bot_ip=sc.broadcast(bot_ip.collect())
bc_bot_agents=sc.broadcast(bot_agents.collect())

ip_to_exclude=sc.broadcast(sc.textFile(hdfs_base_dir+"/ods/etl/excluded_ip/*").map(lambda x:x.split('\t')).map(lambda x:x[0]).collect())
url_to_exclude=sc.broadcast(sc.textFile(hdfs_base_dir+"/ods/etl/excluded_url/*").map(lambda x:x.split('\t')).map(lambda x:x[0]).collect())



try:
    if os.path.isfile(last_extract_value_file_name):
        last_extract_value = open(last_extract_value_file_name,'r').readline().split("\n")[0]
    else:
        last_extract_value='1900-01-01 00:00:00.000000-08:00'
        last_extract_value_file=open(last_extract_value_file_name,'w')
        last_extract_value_file.write(str(last_extract_value))
        last_extract_value_file.close()
except Exception:
    step3='Error reading from last extract value file!!!'
else:
    step3="Successfully read the last extract value file"

print "*************************************************************************************************************"
print "*************************************************************************************************************"
print "*************************************************************************************************************"


file_path=hdfs_base_dir+"/ods/dealwalletcom/event_details/"

today=(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)))

last_extracted_date=datetime.datetime.strptime(last_extract_value[:26],'%Y-%m-%d %H:%M:%S.%f')
today=(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone))).replace(tzinfo=None)
day_before_last_extracted_date=(last_extracted_date+datetime.timedelta(days=-1))
start_date=datetime.datetime(day_before_last_extracted_date.year,day_before_last_extracted_date.month,day_before_last_extracted_date.day)
end_date=datetime.datetime(today.year,today.month,today.day)
datediff=(end_date - start_date).days

date_to_process=[]
files=[]

for i in range (datediff+1):
    print (start_date+datetime.timedelta(days=i))
    date_to_process.append((start_date+datetime.timedelta(days=i)).strftime('%Y-%m-%d'))
    files.append(file_path+str((start_date+datetime.timedelta(days=i)).strftime('%Y-%m-%d'))+"*")
    return_code=commands.getstatusoutput('hadoop fs -touchz '+file_path+str((start_date+datetime.timedelta(days=i)).strftime('%Y-%m-%d'))+'_3')

print "Dates to process : "+",".join(date_to_process)
print "Files to process : "+",".join(files)


extract_start_ts=str((last_extracted_date+datetime.timedelta(hours=-3)).strftime('%Y-%m-%d %H:%M:%S.%f'))+"+00:00"

print "Delta extract Start Time : "+extract_start_ts

print "*************************************************************************************************************"

print "*************************************************************************************************************"

event= sc.textFile(",".join(files)).map(lambda e: e.split('\t'))
visit_event=event.filter(lambda t:(t[2] =='85')).filter(lambda v:len(v)>41).filter(lambda e:e[1]>=extract_start_ts )
# 0 ods_inserted_ts, 1 ods_inserted_ts_utc, 2 Entity_ID, 3 Entity_Name, 4 Entity_desc, 5 Entity_frn_table, 6 Entity_Instance_Column, 7 Entity_Filter_Column, 8 Entity_Filter_Value, 9 even_id, 10 even_event_id, 11 even_entity_id, 12 even_instance_id, 13 even_dynamic_entity_id, 14 even_dynamic_entry, 15 event_entity_db_created_ts, 16 event_id, 17 event_date, 18 user_id, 19 non_member_id, 20 event_type, 21 request_id, 22 session_id, 23 source, 24 event_db_created_ts, 25 et_id, 26 et_name, 27 et_description, 28 et_frn_table_name, 29 et_frn_column_name, 30 visit_id, 31 source_id, 32 search_keyword_id, 33 referrer_id, 34 request_url_stem, 35 request_url_query, 36 browseragent, 37 browser_lang, 38 visit_db_created_ts, 39 query, 40 url, 41 referrer_db_created_ts



visit_event_dedup=visit_event.keyBy(lambda e: e[22]).reduceByKey(lambda v1,v2:getValFromMaxKey(v1[1], v1, v2[1],v2)).map(lambda (k,v):v)


login_signup_event_dup = event.filter(lambda t:(t[20] =='5' or t[20] =='4'))

login_signup_event= login_signup_event_dup.keyBy(lambda e: e[16]).reduceByKey(lambda v1,v2:getValFromMaxKey(v1[1], v1, v2[1],v2)).map(lambda (k,v):v)

login_event = login_signup_event.filter(lambda t:(t[20] =='5'))
signup_event = login_signup_event.filter(lambda t:(t[20] =='4'))
login_event_keyby_session_id=login_event.map(lambda v:(v[22],v[18])).keyBy(lambda v:v[0]).reduceByKey(lambda v1,v2:v1<v2 and v1 or v2)
signup_event_keyby_session_id=signup_event.map(lambda v:(v[22],v[18])).keyBy(lambda v:v[0]).reduceByKey(lambda v1,v2:v1<v2 and v1 or v2)

visit_event_flip_ods_ts=visit_event_dedup.map(lambda v:(v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37],v[38],v[39],v[40],v[41],v[0],v[1]))

visit_event_step_1=visit_event_flip_ods_ts.map(lambda v: (v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],format_ts(v[15][:26]),getNonEmpty(v[16],"0"),v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37],v[38],v[39],v[40],v[41]))
# 0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 user_id, 17 non_member_id, 18 event_type, 19 request_id, 20 session_id, 21 source, 22 event_db_created_ts, 23 et_id, 24 et_name, 25 et_description, 26 et_frn_table_name, 27 et_frn_column_name, 28 visit_id, 29 source_id, 30 search_keyword_id, 31 referrer_id, 32 request_url_stem, 33 request_url_query, 34 browseragent, 35 browser_lang, 36 visit_db_created_ts, 37 query, 38 url, 39 referrer_db_created_ts, 40 ods_inserted_ts, 41 ods_inserted_ts_utc

visit_event_step_2=visit_event_step_1.map(lambda v: (v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15], datetime.datetime.strptime(v[15],'%Y-%m-%d %H:%M:%S.%f').strftime('%Y%m%d'),datetime.datetime.strptime(v[15],'%Y-%m-%d %H:%M:%S.%f').strftime('%H:%M:%S') ,v[16], coalesce(v[17],'-1'), hash_md5(v[17]),v[17][:len(v[17])-17],v[18],v[19], coalesce(v[20],'-1') ,hash_md5(v[20]) ,v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37],v[38],v[39],v[40],v[41]))
# 0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 event_date_id, 17 event_time  , 18 user_id, 19 non_member_id, 20 hash_non_member_id, 21 ip_address   ,   22 event_type, 23 request_id, 24 session_id, 25 hash_session_id, 26 source, 27 event_db_created_ts, 28 et_id, 29 et_name, 30 et_description, 31 et_frn_table_name, 32 et_frn_column_name, 33 visit_id, 34 source_id, 35 search_keyword_id, 36 referrer_id, 37 request_url_stem, 38 request_url_query, 39 browseragent, 40 browser_lang, 41 visit_db_created_ts, 42 query, 43 url, 44 referrer_db_created_ts, 45 ods_inserted_ts, 46 ods_inserted_ts_utc



##Bot filtering logic


visit_keyby_session_id=visit_event_step_2.keyBy(lambda v:v[24])
visit_login = visit_keyby_session_id.leftOuterJoin(login_event_keyby_session_id)
visit_signup= visit_login.leftOuterJoin(signup_event_keyby_session_id)




visit_event_step3 = visit_signup.map(lambda (k,((v,l),s)):(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],s[1] if s is not None else l[1] if l is not None else v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37],v[38],v[39],v[40],v[41],v[42],v[43],v[44],v[45],v[46],is_signup(s),is_login(l)))
# 0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 event_date_id, 17 event_time  , 18 user_id, 19 non_member_id, 20 hash_non_member_id, 21 ip_address   ,   22 event_type, 23 request_id, 24 session_id, 25 hash_session_id, 26 source, 27 event_db_created_ts, 28 et_id, 29 et_name, 30 et_description, 31 et_frn_table_name, 32 et_frn_column_name, 33 visit_id, 34 source_id, 35 search_keyword_id, 36 referrer_id, 37 request_url_stem, 38 request_url_query, 39 browseragent, 40 browser_lang, 41 visit_db_created_ts, 42 query, 43 url, 44 referrer_db_created_ts, 45 ods_inserted_ts, 46 ods_inserted_ts_utc, 47 is_signup, 48 is_login

#Extract Parameter Step 1

visit_event_step4 = visit_event_step3.map(lambda v:(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37],v[38],v[39],v[40],v[41],v[42],v[43],v[44],v[45],v[47],v[48],extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'eeid='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'eeid='), getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'src=') , extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'sem_src=')),getNonEmpty(extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'src='), extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'sem_src=')), extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'acct='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'acct='), getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'camp='),extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'campid=')),getNonEmpty(extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'camp='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'campid=')), extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utype='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utype='), extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'product='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'product='),  extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'gglnum='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'gglnum='), extract_referrer_id_param(coalesce(v[37],'').lower()+coalesce(v[38],'').lower()),extract_referrer_id_param(coalesce(v[43],'').lower()+coalesce(v[42],'').lower()), extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'kw='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'kw='), getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'adg='),extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'adgid=')),getNonEmpty(extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'adg='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'adgid=')), extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'MT='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'MT=') ,extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'q='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'q='), extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'sourceName='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'sourceName='),getDomain(coalesce(v[37],'')+'?'+coalesce(v[38],'')), getDomain(coalesce(v[43],'')+'?'+coalesce(v[42],'')),v[46],getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_medium='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_medium=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_campaign='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_campaign=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_source='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_source=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_content='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_content=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_term='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_term=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_size='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_size=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_pub='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_pub=')),getNonEmpty(extract_url_param(coalesce(v[37],'')+coalesce(v[38],''),'utm_ebs='),extract_url_param(coalesce(v[43],'')+coalesce(v[42],''),'utm_ebs=')),isAllURLParamsPresent(v[38],v[42])))

visit_event_step5 = visit_event_step4.map(lambda v:(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35] ,v[36],v[37],v[38],v[39],v[40],v[41],v[42],v[43],v[44],v[45],v[46],v[47]  ,coalesce(v[48],v[49]),coalesce(v[50],v[51]) , coalesce(v[52],v[53]) , coalesce(v[54],v[55]) , coalesce(v[56],v[57]), coalesce(v[58],v[59]) , coalesce(v[60],v[61]), coalesce(v[62],v[63]), coalesce(v[64],v[65])   , coalesce(v[66],v[67]), coalesce(v[68],v[69]), coalesce(v[70],v[71])    , coalesce(v[72],v[73]) , v[74], v[75] ,v[76],v[77],v[78],v[79],v[80],v[81],v[82],v[83],v[84],v[85]))
# 0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 event_date_id, 17 event_time  , 18 user_id, 19 non_member_id, 20 hash_non_member_id, 21 ip_address   ,   22 event_type, 23 request_id, 24 session_id, 25 hash_session_id, 26 source, 27 event_db_created_ts, 28 et_id, 29 et_name, 30 et_description, 31 et_frn_table_name, 32 et_frn_column_name, 33 visit_id, 34 source_id, 35 search_keyword_id, 36 referrer_id, 37 request_url_stem, 38 request_url_query, 39 browseragent, 40 browser_lang, 41 visit_db_created_ts, 42 query, 43 url, 44 referrer_db_created_ts, 45 ods_inserted_ts, 46 is_signup, 47 is_login, 48 eeid, 49 src, 50 acct, 51 camp, 52 utype, 53 product, 54 gglnum, 55 referrerid, 56 kw, 57 adg, 58 MT, 59 query,60 sourceName, 61 landing_url_domain,62 referring_url_domain, 63 ods_inserted_ts_utc, 64=utm_medium, 65=utm_campaign, 66=utm_source, 67=utm_content, 68=utm_term, 69=utm_size, 70=utm_pub, 71=utm_ebs, 72=url_format_2016

visit_event_step6 = visit_event_step5.map(lambda v:(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37],v[38],v[39],v[40],v[41],v[42],v[43],v[44],v[45],v[46],v[47],v[48],url_string_cleanup(v[49]), url_string_cleanup(v[50]) , url_string_cleanup(v[51]), url_string_cleanup(v[52]), url_string_cleanup(v[53]), v[54], v[55], url_string_cleanup(v[56]), url_string_cleanup(v[57]), v[58], url_string_cleanup(v[59]), v[60], v[61], v[62],ip_location(v[21])[1] , ip_location(v[21])[2] , ip_location(v[21])[3] , ip_location(v[21])[4] , ip_location(v[21])[5] , ip_location(v[21])[6] , getOSBrowser(v[39], v[60])[1], getOSBrowser(v[39], v[60])[2], getOSBrowser(v[39], v[60])[3], getOSBrowser(v[39], v[60])[4], getOSBrowser(v[39], v[60])[5], getOSBrowser(v[39], v[60])[6], getOSBrowser(v[39], v[60])[7], getOSBrowser(v[39], v[60])[8], getOSBrowser(v[39], v[60])[9], getOSBrowser(v[39], v[60])[10], v[63], v[64], v[65], v[66], v[67], v[68], v[69], v[70], v[71], v[72]))
# 0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 event_date_id, 17 event_time  , 18 user_id, 19 non_member_id, 20 hash_non_member_id, 21 ip_address   ,   22 event_type, 23 request_id, 24 session_id, 25 hash_session_id, 26 source, 27 event_db_created_ts, 28 et_id, 29 et_name, 30 et_description, 31 et_frn_table_name, 32 et_frn_column_name, 33 visit_id, 34 source_id, 35 search_keyword_id, 36 referrer_id, 37 request_url_stem, 38 request_url_query, 39 browseragent, 40 browser_lang, 41 visit_db_created_ts, 42 referring_query, 43 referring_url, 44 referrer_db_created_ts, 45 ods_inserted_ts, 46 is_signup, 47 is_login, 48 eeid, 49 src, 50 acct, 51 camp, 52 utype, 53 product, 54 gglnum, 55 referrerid, 56 kw, 57 adg, 58 MT, 59 query,60 sourceName, 61 landing_url_domain,62 referring_url_domain,  63 ip_location_id, 64 country_short, 65 country_long, 66 region, 67 city, 68 zipcode,69 OS_browser_id, 70 device_family, 71 os_family, 72 os_version_string, 73 browser_family, 74 browser_version_string, 75 is_bot, 76 is_mobile,77 is_tablet,78 is_pc, 79 ods_inserted_ts_utc, 80=utm_medium, 81=utm_campaign, 82=utm_source, 83=utm_content, 84=utm_term, 85=utm_size, 86=utm_pub, 87=utm_ebs, 88=url_format_2016

visit_event_step7 =visit_event_step6.map(lambda v:(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37], v[38] , v[39], v[40], v[41], v[42], v[43], v[44], v[45], v[46], v[47], v[48], v[49], v[50], v[51], v[52], v[53], v[54], v[55], v[56], v[57], v[58], v[59], v[60], v[61], v[62], v[63], v[64],v[65],v[66], v[67], v[68], v[69], v[70], v[71], v[72], v[73], v[74],v[75],v[76],v[77],v[78] ,isBranded(v[59],branded_kw), isTrue(isPatternInString(coalesce(v[43],'')+coalesce(v[42],''),'gglnum='),isPatternInString(coalesce(v[37],'')+coalesce(v[38],''),'gglnum=')), isTrue(isPatternInString(coalesce(v[43],'')+coalesce(v[42],''),'product=true'),isPatternInString(coalesce(v[37],'')+coalesce(v[38],''),'product=true')), isTrue(is_referrer_id_in_string(coalesce(v[43],'').lower()+coalesce(v[42],'').lower()),is_referrer_id_in_string(coalesce(v[37],'').lower()+coalesce(v[38],'').lower())), decodeExpr(isPatternInString(v[49],'googadw'),"Google", isPatternInString(v[49],'msn'),"MSN",isPatternInString(v[49],'yahoo'),"Yahoo",isPatternInString(v[49],'gemini'),"Yahoo",isPatternInString(v[49],'criteo'),"Criteo",isPatternInString(v[49],'yandex '),"Yandex "),getPaidMarketingPartner(v[49],v[50],paid_marketing_accounts), getSocialMediaChannel(v[49],v[50],coalesce(v[43],'')+coalesce(v[42],''), social_media_param), getSEONetwork(coalesce(v[43],'')+coalesce(v[42],''),seo_url_pattern), getLeadgen(v[52],leadgen_utype), isBranded(v[56],branded_kw),decodeExpr(coalesce(v[43].strip(),v[42].strip(),None) is not None and 'dealwallet.com' not in v[62] and v[62]<>'' , True, True,False),v[79], v[80], v[81], v[82], v[83], v[84], v[85], v[86], v[87], v[88]))

# 0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 event_date_id, 17 event_time  , 18 user_id, 19 non_member_id, 20 hash_non_member_id, 21 ip_address   ,   22 event_type, 23 request_id, 24 session_id, 25 hash_session_id, 26 source, 27 event_db_created_ts, 28 et_id, 29 et_name, 30 et_description, 31 et_frn_table_name, 32 et_frn_column_name, 33 visit_id, 34 source_id, 35 search_keyword_id, 36 referrer_id, 37 request_url_stem, 38 request_url_query, 39 browseragent, 40 browser_lang, 41 visit_db_created_ts, 42 referring_query, 43 referring_url, 44 referrer_db_created_ts, 45 ods_inserted_ts, 46 is_signup, 47 is_login, 48 eeid, 49 src, 50 acct, 51 camp, 52 utype, 53 product, 54 gglnum, 55 referrerid, 56 kw, 57 adg, 58 MT, 59 query,60 sourceName, 61 landing_url_domain,62 referring_url_domain,  63 ip_location_id, 64 country_short, 65 country_long, 66 region, 67 city, 68 zipcode,69 OS_browser_id, 70 device_family, 71 os_family, 72 os_version_string, 73 browser_family, 74 browser_version_string, 75 is_bot, 76 is_mobile,77 is_tablet,78 is_pc, 79  search_type, 80  is_gglnum, 81 is_product,  82 is_referral,  83 sem_network, 84 paid_marketing_partner   ,  85 social_media, 86 seo_network, 87 leadgen,  88  kw_type,  89 is_weblinks, 90 ods_inserted_ts_utc, 91=utm_medium, 92=utm_campaign, 93=utm_source, 94=utm_content, 95=utm_term, 96=utm_size, 97=utm_pub, 98=utm_ebs, 99=url_format_2016

visit_event_step8 = visit_event_step7.map(lambda v:(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37], v[38] , v[39], v[40], v[41], v[42], v[43], v[44], v[45], v[46], v[47], v[48], v[49], v[50], v[51], v[52], v[53], v[54], v[55], v[56], v[57], v[58], v[59], v[60], v[61], v[62], v[63], v[64],v[65],v[66],v[67], v[68], v[69], v[70], v[71], v[72], v[73], v[74], v[75], v[76], v[77], v[78], v[79], v[80], v[81], v[82], v[83], v[84], v[85], v[86], v[87], v[88], v[89],decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],str(v[93]),str(v[94]),v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50]))[0],decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],str(v[93]),str(v[94]),v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50]))[1], decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],str(v[93]),str(v[94]),v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50]))[2], decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],str(v[93]),str(v[94]),v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50]))[3],  getVisitApplication(v[60],v[39],v[48])[0],getVisitApplication(v[60],v[39],v[48])[1],getVisitApplication(v[60],v[39],v[48])[2], v[90], v[91], v[92], v[93], v[94], v[95], v[96], v[97], v[98], v[99]))


# visit_event_step8 = visit_event_step7.map(lambda v:(v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[26],v[27],v[28],v[29],v[30],v[31],v[32],v[33],v[34],v[35],v[36],v[37], v[38] , v[39], v[40], v[41], v[42], v[43], v[44], v[45], v[46], v[47], v[48], v[49], v[50], v[51], v[52], v[53], v[54], v[55], v[56], v[57], v[58], v[59], v[60], v[61], v[62], v[63], v[64],v[65],v[66],v[67], v[68], v[69], v[70], v[71], v[72], v[73], v[74], v[75], v[76], v[77], v[78], v[79], v[80], v[81], v[82], v[83], v[84], v[85], v[86], v[87], v[88], v[89],isNoneStr_to_blank(getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50]),0),isNoneStr_to_blank(decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],v[93],v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50])),1), isNoneStr_to_blank(decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],v[93],v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50])),2), isNoneStr_to_blank(decodeExpr('true'==str(v[99]).lower(),getVisitTrafficSourceFromURLParams(v[91],v[92],v[93],v[37]+v[38],v[43]+v[42],v[56],branded_kw),'True',getVisitTrafficSource(v[18], QAMembers, v[82], v[48], email_eeid, v[60], v[83], v[80], v[81],v[88], v[84], v[85], v[87], v[86] , v[79], v[89],v[39],v[49],v[50])),3),  getVisitApplication(v[60],v[39],v[48])[0],getVisitApplication(v[60],v[39],v[48])[1],getVisitApplication(v[60],v[39],v[48])[2], v[90], v[91], v[92], v[93], v[94], v[95], v[96], v[97], v[98], v[99]))

#0 Entity_ID, 1 Entity_Name, 2 Entity_desc, 3 Entity_frn_table, 4 Entity_Instance_Column, 5 Entity_Filter_Column, 6 Entity_Filter_Value, 7 even_id, 8 even_event_id, 9 even_entity_id, 10 even_instance_id, 11 even_dynamic_entity_id, 12 even_dynamic_entry, 13 event_entity_db_created_ts, 14 event_id, 15 event_date, 16 event_date_id, 17 event_time  , 18 user_id, 19 non_member_id, 20 hash_non_member_id, 21 ip_address   ,   22 event_type, 23 request_id, 24 session_id, 25 hash_session_id, 26 source, 27 event_db_created_ts, 28 et_id, 29 et_name, 30 et_description, 31 et_frn_table_name, 32 et_frn_column_name, 33 visit_id, 34 source_id, 35 search_keyword_id, 36 referrer_id, 37 request_url_stem, 38 request_url_query, 39 browseragent, 40 browser_lang, 41 visit_db_created_ts, 42 referring_query, 43 referring_url, 44 referrer_db_created_ts, 45 ods_inserted_ts, 46 is_signup, 47 is_login, 48 eeid, 49 src, 50 acct, 51 camp, 52 utype, 53 product, 54 gglnum, 55 referrerid, 56 kw, 57 adg, 58 MT, 59 query,60 sourceName, 61 landing_url_domain,62 referring_url_domain,  63 ip_location_id, 64 country_short, 65 country_long, 66 region, 67 city, 68 zipcode,69 OS_browser_id, 70 device_family, 71 os_family, 72 os_version_string, 73 browser_family, 74 browser_version_string, 75 is_bot, 76 is_mobile,77 is_tablet,78 is_pc, 79  search_type, 80  is_gglnum, 81 is_product,  82 is_referral,  83 sem_network, 84 paid_marketing_partner   ,  85 social_media, 86 seo_network, 87 leadgen,  88  kw_type,  89 is_weblinks, 90 traffic_source_id, 91 traffic_source, 92 traffic_source_type, 93 traffic_source_subtype, 94 application_id, 95 application_type, 96 application_subtype, 97 ods_inserted_ts_utc, 98=utm_medium, 99=utm_campaign, 100=utm_source, 101=utm_content, 102=utm_term, 103=utm_size, 104=utm_pub, 105=utm_ebs, 106=url_format_2016


dw_inserted_ts=str(datetime.datetime.fromtimestamp(time.time()))
dw_inserted_ts_utc = str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)))




visit_event_step9=visit_event_step8.map(lambda v:(dw_inserted_ts,dw_inserted_ts_utc,v[45],v[97],v[14],v[15],v[16],v[17],v[18],v[19],v[20],v[21],v[22],v[23],v[24],v[25],v[0],v[33],v[36],v[37],v[38],v[39],v[40],v[42],v[43],v[46],v[47],v[61],v[62],v[63],v[64],v[65],v[66],v[67],v[68],v[69],v[70],v[71],v[72],v[73],v[74],v[75],v[76],v[77],v[78],v[90],v[91],v[92],v[93],v[94],v[95],v[96],v[48],v[49],v[50],v[51],v[52],v[53],v[54],v[55],v[56],v[57],v[58],v[59],v[60],v[26],v[34],v[35],v[27],v[41],v[44],is_IPbot(v[21],v[16], bc_bot_ip), is_UAbot(v[75], v[39],v[21],v[37], bc_bot_agents, ip_to_exclude, url_to_exclude, v[38]), getDevicePlatform(v[70],v[71],v[76],v[77]),'\\N', v[98], v[99], v[100], v[101], v[102], v[103], v[104], v[105], v[106]))


#Get the current PT timestamp to generate the data directory name

load_time=(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)))
load_time_pst=(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())))
dir_name_ts=str(load_time_pst).replace("-","").replace(" ","").replace(":","").replace(".","")

#Insert the data as a tab delimited file in the data integration layer

visit_event_step10=visit_event_step9.map(lambda v:(cleanformat(v[0])+unicode("\t")+cleanformat(v[1])+unicode("\t")+cleanformat(v[2])+unicode("\t")+cleanformat(v[3])+unicode("\t")+cleanformat(v[4])+unicode("\t")+cleanformat(v[5])+unicode("\t")+cleanformat(v[6])+unicode("\t")+cleanformat(v[7])+unicode("\t")+cleanformat(v[8])+unicode("\t")+cleanformat(v[9])+unicode("\t")+cleanformat(v[10])+unicode("\t")+cleanformat(v[11])+unicode("\t")+cleanformat(v[12])+unicode("\t")+cleanformat(v[13])+unicode("\t")+cleanformat(v[14])+unicode("\t")+cleanformat(v[15])+unicode("\t")+cleanformat(v[16])+unicode("\t")+cleanformat(v[17])+unicode("\t")+cleanformat(v[18])+unicode("\t")+cleanformat(v[19])+unicode("\t")+cleanformat(v[20])+unicode("\t")+cleanformat(v[21])+unicode("\t")+cleanformat(v[22])+unicode("\t")+cleanformat(v[23])+unicode("\t")+cleanformat(v[24])+unicode("\t")+cleanformat(v[25])+unicode("\t")+cleanformat(v[26])+unicode("\t")+cleanformat(v[27])+unicode("\t")+cleanformat(v[28])+unicode("\t")+cleanformat(v[29])+unicode("\t")+cleanformat(v[30])+unicode("\t")+cleanformat(v[31])+unicode("\t")+cleanformat(v[32])+unicode("\t")+cleanformat(v[33])+unicode("\t")+cleanformat(v[34])+unicode("\t")+cleanformat(v[35])+unicode("\t")+cleanformat(v[36])+unicode("\t")+cleanformat(v[37])+unicode("\t")+cleanformat(v[38])+unicode("\t")+cleanformat(v[39])+unicode("\t")+cleanformat(v[40])+unicode("\t")+cleanformat(v[41])+unicode("\t")+cleanformat(v[42])+unicode("\t")+cleanformat(v[43])+unicode("\t")+cleanformat(v[44])+unicode("\t")+cleanformat(v[45])+unicode("\t")+cleanformat(v[46])+unicode("\t")+cleanformat(v[47])+unicode("\t")+cleanformat(v[48])+unicode("\t")+cleanformat(v[49])+unicode("\t")+cleanformat(v[50])+unicode("\t")+cleanformat(v[51])+unicode("\t")+cleanformat(v[52])+unicode("\t")+cleanformat(v[53])+unicode("\t")+cleanformat(v[54])+unicode("\t")+cleanformat(v[55])+unicode("\t")+cleanformat(v[56])+unicode("\t")+cleanformat(v[57])+unicode("\t")+cleanformat(v[58])+unicode("\t")+cleanformat(v[59])+unicode("\t")+cleanformat(v[60])+unicode("\t")+cleanformat(v[61])+unicode("\t")+cleanformat(v[62])+unicode("\t")+cleanformat(v[63])+unicode("\t")+cleanformat(v[64])+unicode("\t")+cleanformat(v[65])+unicode("\t")+cleanformat(v[66])+unicode("\t")+cleanformat(v[67])+unicode("\t")+cleanformat(v[68])+unicode("\t")+cleanformat(v[69])+unicode("\t")+cleanformat(v[70])+unicode("\t")+cleanformat(v[71]) +unicode("\t")+cleanformat(v[72])+unicode("\t")+cleanformat(v[73])+unicode("\t")+cleanformat(v[74])+unicode("\t")+cleanformat(v[75])+unicode("\t")+cleanformat(v[76])+unicode("\t")+cleanformat(v[77])+unicode("\t")+cleanformat(v[78])+unicode("\t")+cleanformat(v[79])+unicode("\t")+cleanformat(v[80])+unicode("\t")+cleanformat(v[81])+unicode("\t")+cleanformat(v[82])+unicode("\t")+cleanformat(v[83])))
visit_event_final=visit_event_step10.coalesce(80).persist(storageLevel=StorageLevel(True,True,False,True,1))
visit_event_final.saveAsTextFile(hdfs_base_dir+"/load/di/visits/tenant="+tenant+"/load_id="+str(dir_name_ts))




hdfs_cmd ='hdfs dfs -mv '+hdfs_base_dir+"/load/di/visits/tenant="+tenant+"/load_id="+str(dir_name_ts)+' '+hdfs_base_dir+"/di/visits/tenant="+tenant+"/load_type=current/"
print hdfs_cmd
return_code=commands.getstatusoutput(hdfs_cmd)
if return_code[0] <>0 :
    print "Error moving data to DI Layer!!!"
    sys.exit(9999)

hdfs_cmd ='hadoop distcp '+hdfs_base_dir+'/di/visits/tenant='+tenant+"/load_type=current/load_id="+str(dir_name_ts)+' '+hdfs_base_dir+'/di/visits/tenant='+tenant+"/load_id="+str(dir_name_ts)
print hdfs_cmd
return_code=commands.getstatusoutput(hdfs_cmd)
if return_code[0] <>0 :
    print "Error copying data to DI Layer!!!"
    sys.exit(9999)

num_records_loaded=visit_event_final.count()


#Stopping Spark Contect
###############################

sc.stop()


#Update last extract value file
###############################

#try:
#    last_extract_value_file=open(last_extract_value_file_name,'w')
#    last_extract_value_file.write(str(new_last_extract_value))
#    last_extract_value_file.close()
#except Exception:
#    print 'Error while updating last  extract value!!!'
#else:
#    print "last extract value is : "+str(new_last_extract_value)
#    print 'Successfully updated last  extract value...'



#Adding impala partition
###############################
# add_partition_script="impala-shell -i "+impala_server+' -q "alter table di.visits add if not exists partition (tenant=\''+tenant+'\',load_id=\''+str(dir_name_ts)+'\')"'
#
# try_again=1
# return_code=1
# while try_again<4 and return_code<>0:
#     print "Try No: " +str(try_again)
#     print add_partition_script
#     return_code=commands.getstatusoutput(add_partition_script)
#     try_again = try_again+1
#     return_code=return_code[0]
# if return_code <>0 :
#     print "Error in adding partition to impala table!!!"
#     sys.exit(9999)



print "Number of records loaded:"
print num_records_loaded
print "Job completed successfully"

