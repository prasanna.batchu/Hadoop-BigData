===================
Member Profile Job
===================

  The member profile job is responsible for processing the data generated in dealwallet/IRS applications and store it into DW HDFS location in the form of parquet files. The member profile data consists of following tables from Dealwallet/IRS:

1. users
2. users_info
3. payments
4. user_hold_payments
5. shopping trips
6. promotions
7. visits

The whole process was devided into 3 different jobs.

Job1: job1 is responsible for moving the data from postgres to ODS files by using sqoop.
=====

Job2: job2 is responsible for moving data from events logs to dw through ods and di 
=====

Job3: job3 is responsible to aggreagate the data from ODS to DW location in the form of parquet files
=====

The automation scripts created were responsible for 

  1. creating the tables in postgres DB (provided in the configuration) 
  2. generating the valid sample data into these tables
  3. running the sqoop jobs for moving this data into ODS files
  4. generating visits data from events logs to dw through ods and di
  5. running the member_profile spark job to process the data and store it into DW files
  6. running the python scripts for applying various validations including data validation, schema validation, partition validation and etc..
  7. sending the email and hipchat with the status of the job execution

Please follow below step by step execution to understand the whole automation process and replicating the error scenarios and verifying the behaviour of scripts in the error cases:

Step 1: copy the zip file into the bigdata server and extract it to following location "/home/engineering/"

Step 2: 

  once extracted, open the following file "/home/engineering/member_profile_changes/job1_auto_test.sh" and look for the following line

  "python /home/engineering/member_profile_changes/table_creation_pythonscripts/create_data.py <data_sequence_start_id> <no_of_record_to_insert_ineach_table> <name_of_database> <db_username> <db_password> <db_host> <db_port>"

Please update the correct database details like db_name, username, password, host and port

update the email address to your email in the following file "/home/engineering/member_profile_changes/config.properties" => "To" field

update postgres details in the following file "/home/engineering/member_profile_changes/config.properties" => "db_admin_url" field as => jdbc:postgresql://<db_host>:<db_port>/<name_of_database>

update hdfs host and port in member_profile_changes/passingArguments/member.py file at

                             #Configure the host name of the hdfs file system
                             hdfs = HDFileSystem(host='host_name', port=port_num)

create path in hdfs browser like '/user/hue/ods/' and keep the folder '/home/engineering/member_profile_changes/etl' so that hdfs path will be like "/user/hue/ods/etl/"

create path in hdfs browser like '/user/eventlog/' and keep the folder '/home/engineering/member_profile_changes/dealwallet.com/' so that hdfs path will be like '/user/eventlog/dealwallet.com/'.

create path in hdfs browser like '/user/hue/ods/' and keep the folder '/home/engineering/member_profile_changes/dealwalletcom/' so that hdfs path will be like '/user/hue/ods/dealwalletcom/'.

create path in hdfs browser like "/user/hue/di/visits/tenant=dealwallet.com/load_type=current" , "/user/hue/dw/visits/tenant=dealwallet.com" and "/user/hue/dw/Target/target/" before running the script  

make sure server is installed with apache2.

        create 'member' folder in path "/var/www/html/" then 

        create 2 more folders in "/var/www/html/member/" named 'current_run'("/var/www/html/member/current_run") and
                                                               'report'     ("/var/www/html/member/report") then

        create 2 more folders in "/var/www/html/member/current_run" named 'log_files'("/var/www/html/member/current_run/log_files") and 
                                                                            'status'  ("/var/www/html/member/current_run/status")
        give appropriate permission to these folders for the user.

create path in hdfs browser like '/user/hue/' and keep the folder '/home/engineering/member_profile_changes/schema' so that hdfs path will be like '/user/hue/schema/'.

Step 3:

  Now, execute the following command "./home/engineering/member_profile_changes/member_final_job.sh". Here it will execute all the table creation, data generation, sqoop jobs, spark job to process the data, running automation script for validating various test cases. at the end of this job you will get an email and hipchat with job status and log file as attachement for all testcases and postive case.

we created 9 error jars in location '/home/engineering/member_profile_changes/' to test that our automation script catches every error.

Note : This script will execute one positive case along with 9 error test cases.

       1.positive case 
       2.partition error 
       3.sorting error
       4.traffic source error
       5.dw table index error
       6.traffic source type error
       7.tv expanded error
       8.traffic source subtype error
       9.dw visits index error
      10.visit data integration error

Following are the test cases covered in the python scripts :

1. In positive case all tests will be successful and report will be sent to mail

2. In partition error case number of partitions comparision will be failed and error message will be sent on mail.

3. In sorting error case sorting order of data will not match with the original data and error message will be sent on mail.

4. In traffic source error case changing data in traffic source column will lead to change in data of it's dependant column and error message will be sent on mail regarding data mismatch in that column.

5. In dw table index error case shuffling dw member table index will lead to mismatch in data for final member table and error message will be sent on mail regarding data mismatch for that column.

6.In traffic source type error case changing data in traffic source type column will lead to change the data in it's dependant column and error message will be sent on mail regarding data mismatch in that column.

7.In tv expanded error case changing logic in it's column derivation will lead to change the data generated in this column and error message will be sent on mail regarding data mismatch in tv expanded column of member table.

8.In traffic source sub type error case data in traffic source sub type column is changed so that traffic source id column data will be changed and error message will be sent on mail regarding data mismatch in traffic source id column.

9.In dw visits index error case columns of dw visits table were shuffled so that there will be mismatch in final member table columns data and error message will be sent on mail regarding mismatch in data of that column.

10.In visit data integration error case source data to dw visits table will be changed by creating error in dataintegation code which sends data from di to dw so data will be mismatched in the final dw member table and an error message will be sent on mail regarding mismatch in data of that column.

Step 4:

Please visit the following link after completion of job to view the status of all test cases 

                     <server IP address>/member/report/report.html

If any one of the following 5 test cases failed then all the 5 test cases will be failed.
1.Data comparision Test Case
2.Junk data comparison test case
3.dw member table index shuffle test case
4.visit index shuffle comparison test case
5.traffic source input data bug comparison test case

Note :
   To view only the positive data in please run the following spark -submit

time(spark-submit --master yarn --num-executors 2 --executor-memory 6g --driver-memory 1g --executor-cores 6 --conf spark.scheduler.mode=FAIR --conf spark.yarn.queue=low_priority_jobs --conf spark.executor.extraJavaOptions=-XX:+UseG1GC --conf spark.sql.tungsten.enabled=true --class MemberProfileTest --conf spark.yarn.jar=local:/opt/cloudera/parcels/CDH/lib/spark/lib/spark-assembly-1.6.0-cdh5.9.0-hadoop2.6.0-cdh5.9.0.jar member_profile-1.0-SNAPSHOT.jar) 

NOTE: We created dummy data for the automation prototype same as ebates member table data. This automation prototype can also be applicable to ebates membertable if we are having the data for dw.member table.
