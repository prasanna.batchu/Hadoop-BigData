start_time=$(date +%s)

echo "1.member_profile jar execution process successfully started"
echo "member_profile" > /tmp/current_execution.txt
./member_job.sh member_profile-1.0-SNAPSHOT.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/member_profile_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile.log
echo "member_profile jar execution proce completed successfully"

echo "2.member_profile_partition_error jar execution process successfully started"
echo "member_profile_partiton_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_partiton_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/partiton_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_partiton_error.log
echo "member_profilepartition_error jar execution process completed successfully"

echo "3.member_profile_sorting_error jar execution process successfully started"
echo "member_profile_sorting_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_sorting_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/sorting_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_sorting_error.log
echo "member_profile_sorting_error execution process completted successfully"

echo "4.member_profile_dw_table_index_error jar execution process started successfully"
echo "member_profile_dw_table_index_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_dw_table_index_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/dw_table_index_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_dw_table_index_error.log
echo "member_profile_dw_table_index_error jar execution process completed successfully"

echo "5.member_profile_trafficsource_error jar execution process started successfully"
echo "member_profile_trafficsource_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_trafficsource_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/trafficsource_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_trafficsource_error.log
echo "member_profile_trafficsource_error jar execution process completed successfully"

echo "6.member_profile_trafficsource_subtype_error jar execution process started successfully"
echo "member_profile_trafficsource_subtype_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_trafficsource_subtype_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/trafficsource_subtype_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_trafficsource_subtype_error.log
echo "member_profile_trafficsource_subtype_error jar execution process completed successfully"

echo "7.member_profile_trafficsource_typeerror jar execution process started successfully"
echo "member_profile_trafficsource_typeerror" > /tmp/current_execution.txt
./member_job_error.sh member_profile_trafficsource_typeerror.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/trafficsource_typeerror_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_trafficsource_typeerror.log
echo "member_profile_trafficsource_typeerror jar execution process completed successfully"

echo "8.member_profile_tv_expanded_error jar execution process started successfully"
echo "member_profile_tv_expanded_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_tv_expanded_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/tv_expanded_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_tv_expanded_error.log
echo "member_profile_tv_expanded_error jar execution process completed successfully"

echo "9.member_profile_visits_index_error jar execution process started successfully"
echo "member_profile_visits_index_error" > /tmp/current_execution.txt
./member_job_error.sh member_profile_visits_index_error.jar dealwallet_transform_load_dw_visit_dataintegration.py > /var/www/html/member/current_run/log_files/visits_index_error_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/member_profile_visits_index_error.log
echo "member_profile_visits_index_error jar execution process completed successfully"

echo "visit_dataintegration_error jar execution process started successfully"
echo "visit_dataintegration_error" > /tmp/current_execution.txt
./member_job_dataint_error.sh member_profile-1.0-SNAPSHOT.jar dealwallet_transform_load_dw_visit_dataintegration_error.py >/var/www/html/member/current_run/log_files/visit_data_intigration_performance.log
mv /home/engineering/member_profile_changes/memberjob.log /var/www/html/member/current_run/log_files/visit_dataintegration_error.log
echo "visit_dataintegration_error jar execution process completed successfully"

java -cp DataGenerator.jar com.tsss.member.data.TestReportGenerator "/var/www/html/member/"
python sendMail.py
python hipchatlinksend.py

finish_time=$(date +%s)
echo "Time duration: $((finish_time - start_time)) secs."
echo min=$(( $((finish_time - start_time)) /60 )) min.


