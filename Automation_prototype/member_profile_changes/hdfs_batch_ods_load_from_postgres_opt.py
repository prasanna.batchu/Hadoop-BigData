#Importing the required packages
import sys
import time
import datetime
import pytz
import os
import socket, struct
import psycopg2
import commands
import subprocess
import ConfigParser
import traceback



originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

#########################################################
#Checking the number of arguments passed at the runtime #
#########################################################


if len(sys.argv) != 5 and len(sys.argv) !=6 and len(sys.argv) !=7 :
    print >> sys.stderr, "Usage: "+sys.argv[0] +" <tenant> <src_schema> <src_table> <sqoop_job> [<limit> / <start value> <end value>]"
    exit(-1)


#Store the arguments into variables
###################################

if len(sys.argv) == 5:
    script,tenant, src_schema, src_table,sqoop_job = sys.argv

if len(sys.argv) == 6:
    script,tenant, src_schema, src_table ,sqoop_job, limit_count = sys.argv

if len(sys.argv) == 7:
    script,tenant, src_schema, src_table,sqoop_job, start_value, end_value = sys.argv

config = ConfigParser.RawConfigParser()
config.read('config.properties')

user=os.environ["USER"]

# set up parameters
if user not in ['root','engineering','hue']:
    print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
    sys.exit(1234)

else:
    base_dir=config.get('Properties','base_dir')
    hdfs_base_dir = config.get('Properties','hdfs_base_dir')

sqoop_job_run_cmd='sqoop job --exec '+  sqoop_job

#Sqooping data from the database
###########################################

return_code=commands.getstatusoutput(sqoop_job_run_cmd)
print return_code
if return_code[0]<>0:
    print "Error sqooping data!!!"
    print return_code
    sys.exit(9999)

data_file_name=str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)).strftime("%Y%m%d_%H%M%S%f"))
return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+"/ods/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table)
return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name)
return_code=commands.getstatusoutput('hdfs dfs -rm -r '+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/*")
return_code=commands.getstatusoutput('hdfs dfs -mv '+hdfs_base_dir+"/temp/"+tenant + "/"+ src_schema + "_new_" + src_table+"/part* "+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name)
return_code=commands.getstatusoutput('hdfs dfs -mv '+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name+" "+hdfs_base_dir+"/ods/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name)
print return_code
if return_code[0] <>0 :
    print "Error in moving data to ODS!!!"
    sys.exit(9999)

print "Successfully moved the data to ODS ..."










