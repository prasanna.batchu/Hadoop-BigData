#importing the required packages
import os
import sys
import psycopg2
import time
import contextlib
import urllib
import ConfigParser

#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################
if len(sys.argv) != 8 and len(sys.argv) != 9 and len(sys.argv) != 10:
   print >> sys.stderr, "Usage: " + sys.argv[0] + " <no_part> <num_records> <database_name> <user_name> <passwd> <host_name> <port_no> [<limit> / <path1> <path2>]"
   exit(-1)
# Store the arguments into variables
####################################
if len(sys.argv) == 8:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no = sys.argv

if len(sys.argv) == 9:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, limit_count = sys.argv

if len(sys.argv) == 10:
    script, no_part, num_records, database_name, user_name, passwd, host_name, port_no, path1, path2 = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

config = ConfigParser.RawConfigParser()
config.read('config.properties')
user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   partition_range = config.get('Properties','partition_range')

####################################################################################################################################
conn= psycopg2.connect(database=database_name, user=user_name, password=passwd, host=host_name, port=port_no)
print "Opened bigdata database successfully"
cur = conn.cursor()
#DROP THE TABLE promotions
cur.execute('''drop table promotions;''')
conn.commit()
print "successfully drop the table promotions"

#CREATE A TABLE promotions

cur.execute('''create table if not exists promotions (up_id numeric,up_user_id numeric,up_amount numeric,up_approved_date timestamp without time zone,
            up_payment_id numeric,up_state smallint,up_promo_type smallint,up_promo_id numeric,up_qualifying_order_id numeric,up_creation_date timestamp without time zone,
            up_last_modified_date timestamp without time zone,friend_member_id bigint,last_processed_ts timestamp without time zone,up_referee_name character varying,
            modified_by integer);''')
conn.commit()
print "promotions table created successfully in database bigdata with owner postgres"

#INSERT VALUES INTO ALL COLUMNS 
print "INSERTING AND UPDATING THE VALUES INTO THE POSTGRES TABLE promotions" 
for s in range(int(no_part)):
   for j in range(1,int(num_records)+1):
      x = j + s*int(partition_range)
      for i in [x]:
             cur.execute('''INSERT INTO promotions(up_id,up_user_id,up_amount,up_approved_date,up_payment_id,up_state,up_promo_type,up_promo_id,up_qualifying_order_id,
                            up_creation_date,up_last_modified_date,friend_member_id,last_processed_ts,up_referee_name,modified_by)
                     SELECT unnest(array[%s]::numeric[])as up_id,
                            unnest(array[%s]::numeric[]) as up_user_id,
                            unnest(array[10%s]::numeric[]) as up_amount,
                            unnest(array[current_date-13]::timestamp without time zone[]) as up_approved_date,
                            unnest(array[0]::numeric[]) as up_payment_id,
                            unnest(array['1']::smallint[]) as up_state,
                            unnest(array['1' ]::smallint[]) as up_promo_type,
                            unnest(array[6%s]::numeric[]) as up_promo_id,
                            unnest(array['0']::numeric[]) as up_qualifying_order_id,
                            unnest(array[current_date-25]::timestamp without time zone[])as up_creation_date,
                            unnest(array[current_date-22]::timestamp without time zone[]) as up_last_modified_date,
                            unnest(array['2']::bigint[]) as friend_member_id,
                            unnest(array[current_date]::timestamp without time zone[]) as last_processed_ts,
                            unnest(array[' ']::character varying[]) as up_referee_name,
                            unnest(array['2']:: integer[]) as modified_by'''%(i,i,i,i))
conn.commit()
print "SUCCESSFULLY INSERED AND UPDATED THE TABLE PROMOTIONS" 

