#This is the HDFS script to fetch data from postgres database and pushes to hdfs  #
#                                                                                                  #
####################################################################################################


#Importing the required packages
#################################

import sys
import time
import datetime
import pytz
import os
import socket, struct
import psycopg2
import commands
import subprocess
import ConfigParser
import traceback

#########################################################
#Checking the number of arguments passed at the runtime #
#########################################################


if len(sys.argv) != 6 and len(sys.argv) !=7 and len(sys.argv) !=8 :
    print >> sys.stderr, "Usage: "+sys.argv[0] +" <tenant> <platform> <version>  <src_schema> <src_table> [<limit> / <start value> <end value>]"
    exit(-1)


#Store the arguments into variables
###################################

if len(sys.argv) == 6:
    script,tenant,platform,version, src_schema, src_table = sys.argv

if len(sys.argv) == 7:
    script,tenant,platform,version, src_schema, src_table , limit_count = sys.argv

if len(sys.argv) == 8:
    script,tenant,platform,version,src_schema, src_table, start_value, end_value = sys.argv


originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

user=os.environ["USER"]


config = ConfigParser.RawConfigParser()
config.read('config.properties')

# set up parameters
if user not in ['engineering']:
    print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
    sys.exit(1234)

else:
    base_dir=config.get('Properties','base_dir')
    hdfs_base_dir = config.get('Properties','hdfs_base_dir')
    db_admin_url=config.get('Properties','db_admin_url')
   # data_dir = config.get('Properties','data_dir')
   # db_replica_url = config.get('Properties','db_replica_url')
   # event_dburl = config.get('Properties','event_dburl')
   # app_dburl = config.get('Properties','app_dburl')
   # dburl = config.get('Properties','dburl')
   # mysql_dburl = config.get('Properties','mysql_dburl')
   # src_db = config.get('Properties',src_db)
    #src_db_host = config.get('Properties',src_db_host)
   # event2_dburl = config.get('Properties','event2_dburl')
   # mysql_username= config.get('Properties','mysql_username')
   # mysql_password= config.get('Properties','mysql_password')
    #dburl2 = config.get('Properties','dburl2')
    #ebdev2_dburl = config.get('Properties','ebdev2_dburl')


#Fetch the SQL and the last extracted value
###########################################
#print "sqoop file:" +base_dir+'/sqoop/'+platform+'/'+version+'/'+src_schema+'.'+src_table+'.sqoop'
print "sqoop file:" +base_dir+'/sqoop/'+platform+'/'+version+'/'+src_schema+'.'+src_table+'.sh'
try:
    #sqoopfile = open(base_dir+'/sqoop/'+platform+'/'+version+'/'+src_schema+'.'+src_table+'.sqoop','r')
    sqoopfile = open(base_dir+'/sqoop/'+platform+'/'+version+'/'+src_schema+'.'+src_table+'.sh','r')

except Exception:
    step2='Error reading from sql file!!!'
    print 'Error reading from sql file!!!'
    traceback.print_exc(file=sys.stdout)
else:
    step2="Successfully read the sql file"
    print "Successfully read the sql file"


    #sqoop_cmd=sqoopfile.readline().strip()
    sqoop_cmd = sqoopfile.read().strip()
    sqoopfile.close()
    sqoop_cmd=sqoop_cmd.replace("$db_admin_url",db_admin_url).replace("$hdfs_base_dir",hdfs_base_dir).replace("$base_dir",base_dir)

print sqoop_cmd
print ""
print "#################################################################################################################"


#Sqooping data from the database
###########################################

return_code=commands.getstatusoutput(sqoop_cmd)
print return_code
if return_code[0]<>0:
    print "Error creating sqoop job!!!"
    print return_code
    sys.exit(9999)

#sqoop_job_run_cmd= 'sqoop job --exec '+  sqoop_job

#return_code=commands.getstatusoutput(sqoop_job_run_cmd)
#print return_code
#if return_code[0]<>0:
    #print "Error sqooping data!!!"
    #print return_code
    #sys.exit(9999)

#data_file_name=str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)).strftime("%Y%m%d_%H%M%S%f"))
#return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+"/ods/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table)
#return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name)
#return_code=commands.getstatusoutput('hdfs dfs -rm -r '+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/*")
#return_code=commands.getstatusoutput('hdfs dfs -mv '+hdfs_base_dir+"/temp/"+tenant + "/"+ src_schema + "_new_" + src_table+"/part* "+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name)
#print return_code
#return_code=commands.getstatusoutput('hdfs dfs -mv '+hdfs_base_dir+"/load/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name+" "+hdfs_base_dir+"/ods/"+tenant.replace(".", "") + "/"+ src_schema + "_new_" + src_table+"/"+data_file_name)
#print return_code
#if return_code[0] <>0 :
    #print "Error in moving data to ODS!!!"
    #sys.exit(9999)


#print ""
#log_time_PT = str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(targetTimeZone)))
#print log_time_PT+":"
#step4="Successfully moved the data to ODS ..."
#print "Successfully moved the data to ODS ..."

