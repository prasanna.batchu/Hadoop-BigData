import sys
import time
import datetime
import pytz
import os
import commands
import uuid
import ConfigParser
from pyspark import SparkContext, SparkConf
from pyspark.storagelevel import StorageLevel

tenant='dealwallet.com'
user=os.environ["USER"]

config = ConfigParser.RawConfigParser()
config.read('config.properties')

# set up parameters
if user not in ['root','engineering']:
    print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
    sys.exit(1234)
else:
    base_dir=config.get('Properties','base_dir')
    hdfs_base_dir = config.get('Properties','hdfs_base_dir')
    impala_server = config.get('Properties','impala_server')

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"
script='dealwallet_events_logs_ods_load'

################################### Start of Attribute ID function ##############################################################

ENTITY_MAP = { '86' : 'Split', '85' : 'Visit', '110' : 'Capture Browser Language', '77' : 'Gumball Container', '76' : 'Gumball', '2' : 'Top Stores', '65' : 'Merchant Sessions', '87' : 'Toolbar ID', '92' : 'Toolbar merchant session', '89' : 'Toolbar Feature', '66' : 'Users', '108' : 'Capture Signup Email', '93' : 'Toolbar Browser', '94' : 'ebates Search', '95' : 'Product Search', '127' : 'BFPS Address and Name email change Request', '128' : 'BFPS Name & Address change successfully updated Email Request', '99' : 'Refer a friend', '17' : 'Toolbar Status', '105' : 'Credit Card', '106' : 'Login Failure', '107' : 'Cash Back Method Change', '114' : 'PayPal Address Change Email Request', '115' : 'Registered Email Address Change Mail Request', '109' : 'Email Address Change', '111' : 'Create Password Prompt', '112' : 'Create Password Email', '113' : 'Create Password Complete', '116' : 'SEM Visit', '129' : 'SEM Click', '150' : 'SEM visit', '152' : 'SEM visit', '122' : 'Address - Guided Help: Big Fat Payment - View', '123' : 'Address - Guided Help: Big Fat Payment - Click', '120' : 'Address - Address Overlay - Click', '121' : 'Address - Address Overlay - Complete', '119' : 'Address - Address Overlay - View', '125' : 'Merchant Session - AdBlock Plus enabled', '130' : 'Cashstar - Sign-In', '134' : 'Change Password - Request', '135' : 'Change Password - Complete', '136' : 'Reset Password - Request', '137' : 'Reset Password - Complete', '44' :  'Merchant Store Page Visit', '140' : 'CLO - Register consumer', '141' : 'CLO - Delete consumer', '142' : 'CLO - Link offer', '143' : 'CLO - Unlink offer', '144' : 'Credit Card - Add card (EB)', '145' : 'Credit Card - Update card (EB)', '146' : 'Credit Card - Delete card (EB)', '147' : 'Credit Card - Register card (CLO)', '148' : 'Credit Card - Unlink card (CLO)', '149' : 'Credit Card - Set default' }
EVENT_MAP = {'2':'Click', '3':'Merchant Session', '4':'Signup', '5':'Login', '6':'Toolbar Install', '13':'ebates Search', '14':'Product Search', '15':'Address', '16':'Refer a friend', '17':'Toolbar Status', '18':'Credit Card', '19':'Login Failure', '20':'Cash Back Method Change', '21':'Email Address Change', '22':'Create Password', '26':'SEM Click', '27':'Address - Guided Help: Big Fat Payment - View', '28':'Address - Guided Help: Big Fat Payment - Click', '30':'Address - Address Overlay - Click', '31':'Address - Address Overlay - Complete', '32':'Address - Address Overlay - View', '35':'Merchant Session - AdBlock Plus enabled', '37':'Cashstar - Sign-In', '40':'Change Password - Request', '41':'Change Password - Complete', '42':'Reset Password - Request', '43':'Reset Password - Complete', '44':'Merchant Store Page Visit', '46':'CLO - Register consumer', '47':'CLO - Delete consumer', '48':'CLO - Link offer', '49':'CLO - Unlink offer', '50':'Credit Card - Add card (EB)', '51':'Credit Card - Update card (EB)', '52':'Credit Card - Delete card (EB)', '53':'Credit Card - Register card (CLO)', '54':'Credit Card - Unlink card (CLO)', '55':'Credit Card - Set default', '58':'Profile Update Verification'}
EVENT_MAP_VALUE_AS_KEY = {'Address - Guided Help: Big Fat Payment - View': '27', 'Address - Guided Help: Big Fat Payment - Click': '28', 'Address - Address Overlay - Click': '30', 'Address - Address Overlay - Complete': '31', 'Address - Address Overlay - View': '32', 'Merchant Session - AdBlock Plus enabled': '35', 'Cashstar - Sign-In': '37', 'Change Password - Request': '40', 'Merchant Store Page Visit': '44', 'Video - Cash Back Made Simple': '56', 'Video - In-Store Cash Back': '57', 'Profile Update Verification': '58', 'View - ebates Welcome page': '59', 'View - ebates test homepage': '60', 'View - ebates Luxury page': '61'}

def get_entity_type(entityTypeId):
    try :
        return ENTITY_MAP[entityTypeId]
    except Exception:
        return 'Unkown'

def get_event_type(eventTypeId):
    try :
        return [eventTypeId, EVENT_MAP[eventTypeId]]
    except Exception:
        try :
            return [EVENT_MAP_VALUE_AS_KEY[eventTypeId], eventTypeId]
        except Exception:
            return [eventTypeId,'Unknown']

################################### End of Attribute ID Function##############################################################

def coalesce(*arg):
    for el in arg:
        if el is not None and el <>'None' and el <>'NULL' and el <> 'null'  and el <> 'Null' and el <> "\\N":
            return el
    return None

def getNonEmpty(*arg):
    for el in arg:
        if el is not None and el <>'None' and el.lower() <>'null' and el <> "\\N" and el.strip()<>'':
            return el
    return unicode( "\\\\N")

def extract_url_param( url, param, startdelim='&' , enddelim='&'  ):
    if url is None or url =='':
        return None
    if param is None or param=='':
        return None
    try:
        start = url.index( startdelim+param ) + len( startdelim+param )
        try:
            end = url.index( enddelim, start )
        except ValueError:
            end = len(url)
        return url[start:end]
    except ValueError:
        return None

def extract_data(tag_str,meta, ods_inserted_ts, ods_inserted_ts_utc):
    if tag_str is None or meta is None:
        return None
    result=[]
    result.append(ods_inserted_ts)
    result.append(ods_inserted_ts_utc)
    result.append(uuid.uuid1().int)  #Generating Unique Event ID
    for m in range(len(meta)):
        if meta[m]=="entityTypeId=":
            entityTypeId = extract_url_param(tag_str,meta[m],"\t" ,"\t")
            result.append(entityTypeId)
            result.append(get_entity_type(entityTypeId))
        elif meta[m]=="eventType=":
            list = get_event_type(extract_url_param(tag_str,meta[m],"\t","\t"))
            result.append(list[0])
            result.append(list[1])
        else:
            result.append(extract_url_param(tag_str,meta[m],"\t","\t"))
    if result[3] is None:
        result[3]=result[4]
    return result

def listToString(list,delimiter="\t"):
    if list is None:
        return None
    row=''
    for l in range(len(list)):
        row=row+coalesce(unicode(list[l]),'')+delimiter
    return row

conf = (SparkConf().setAppName("dealwallet_events_logs_ods_load").set("spark.ui.port","0"))
sc = SparkContext(conf = conf)

print "*************************************************************************************************************"
print "*************************************************************************************************************"
print "*************************************************************************************************************"

meta_file=base_dir+"/config/extract_meta/dealwallet.com_event_logs.meta"
meta_tags = [line.rstrip('\n') for line in open(meta_file)]

ods_event_logs_dir = hdfs_base_dir+"/ods/dealwalletcom/event_logs/"
ods_event_details_dir = hdfs_base_dir+"/ods/dealwalletcom/event_details/"
load_event_logs_dir = hdfs_base_dir+"/load/dealwalletcom/event_logs/"
load_event_details_dir = hdfs_base_dir+"/load/dealwalletcom/event_details/"
load_raw_password_event_dir = hdfs_base_dir+"/load/rawpassword/"
raw_password_event_dir = hdfs_base_dir+"/rawpassword/"

return_code=commands.getstatusoutput('hdfs dfs -mkdir -p '+ods_event_logs_dir+' '+ ods_event_details_dir + ' ' + load_event_logs_dir + ' ' + load_event_details_dir + ' ' + load_raw_password_event_dir  + ' ' + raw_password_event_dir)
return_code=commands.getstatusoutput('hdfs dfs -rm -r ' + load_event_logs_dir + '* ' + load_event_details_dir + '* ' + load_raw_password_event_dir + '*')

status, output = commands.getstatusoutput("hdfs dfs -ls /user/eventlog/dealwallet.com/DealwalletEventData_*_*")
output_lines=output.split("\n")
files=[f[f.index("/"):] for f in output_lines]

ods_inserted_ts=str(datetime.datetime.fromtimestamp(time.time()))
ods_inserted_ts_utc = str(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())).astimezone(pytz.timezone(originalTimeZone)))

event_files= sc.textFile(",".join(files)).filter(lambda x:"evtData -" in x).coalesce(900)
passwordEventLogs = event_files.filter(lambda u: 'userRawPassword' in u.split('\t')[0])

#.filter(lambda x:"userRawPassword" in x) #.filter(lambda x:"merchantSessionTracking" in x) ##.filter(lambda x:"merchantSessionId=1002329750" in x)
event_data_extract=event_files.map(lambda u:u.replace('evtData - ', '\t')).map(lambda x: extract_data(x,meta_tags, ods_inserted_ts, ods_inserted_ts_utc))
event_data=event_data_extract.filter(lambda x: (x[3] is not None)).persist(storageLevel=StorageLevel(False,True,False,True,1))

event_date=sc.broadcast(event_data.map(lambda x: x[3][:10]).distinct().collect())
print event_date.value

ods_event_data=event_data.map(lambda e:(e[0], e[1], e[5], e[6], "", "", "", "", "", e[2], e[2], e[5], e[7], e[8], e[9], e[3], e[2], e[3], getNonEmpty(e[10],"0"), e[11], e[12], e[14], e[15], e[17], e[3], e[12], e[13], "", "", "", e[7], e[18], e[19], e[2], e[30], e[29], e[27], e[28], e[3], e[29], e[20], e[3],e[65],e[66],e[67],e[16]))
# 0	ods_inserted_ts, 1	ods_inserted_ts_utc, 2	entity_id, 3	entity_name, 4	entity_desc, 5	entity_frn_table, 6	entity_instance_column, 7	entity_filter_column, 8	entity_filter_value, 9	even_id, 10	even_event_id, 11	even_entity_id, 12	even_instance_id, 13	even_dynamic_entity_id, 14	even_dynamic_entry, 15	event_entity_db_created_ts, 16	event_id, 17	event_date, 18	user_id, 19	non_member_id, 20	event_type, 21	request_id, 22	session_id, 23	source, 24	event_db_created_ts, 25	et_id, 26	et_name, 27	et_description, 28	et_frn_table_name, 29	et_frn_column_name, 30	visit_id, 31	source_id, 32	search_keyword_id, 33	referrer_id, 34	request_url_stem, 35	request_url_query, 36	browseragent, 37	browser_lang, 38	visit_db_created_ts, 39	query, 40	url, 41	referrer_db_created_ts

browser_lang_ods_event_data= ods_event_data.filter(lambda e:(e[2]=='85' and e[20]=='2')).map(lambda e:(e[0], e[1], "110", "Capture Browser Language", e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15], uuid.uuid1().int, e[17], e[18], e[19], e[20], e[21], e[22], e[23], e[24], e[25], e[26], e[27], e[28], e[29], e[30], e[31], e[32], e[33], e[34], e[35], e[36], e[37], e[38], e[39], e[40], e[41],e[42],e[43],e[44],e[45]))
signup_email_ods_event_data= ods_event_data.filter(lambda e:(e[2]=='66' and e[20]=='4')).map(lambda e:(e[0], e[1], "108", "Capture Signup Email", e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15], uuid.uuid1().int, e[17], e[18], e[19], e[20], e[21], e[22], e[23], e[24], e[25], e[26], e[27], e[28], e[29], e[30], e[31], e[32], e[33], e[34], e[35], e[36], e[37], e[38], e[39], e[40], e[41],e[42],e[43],e[44],e[45]))
signup_bl_ods_event_data= ods_event_data.filter(lambda e:(e[2]=='66' and e[20]=='4')).map(lambda e:(e[0], e[1], "110", "Capture Browser Language", e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15], uuid.uuid1().int, e[17], e[18], e[19], e[20], e[21], e[22], e[23], e[24], e[25], e[26], e[27], e[28], e[29], e[30], e[31], e[32], e[33], e[34], e[35], e[36], e[37], e[38], e[39], e[40], e[41],e[42],e[43],e[44],e[45]))
signup_split_ods_event_data= ods_event_data.filter(lambda e:(e[2]=='66' and e[20]=='4')).map(lambda e:(e[0], e[1], "86", "Split", e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15], uuid.uuid1().int, e[17], e[18], e[19], e[20], e[21], e[22], e[23], e[24], e[25], e[26], e[27], e[28], e[29], e[30], e[31], e[32], e[33], e[34], e[35], e[36], e[37], e[38], e[39], e[40], e[41],e[42],e[43],e[44],e[45]))
login_split_ods_event_data= ods_event_data.filter(lambda e:(e[2]=='66' and e[20]=='5')).map(lambda e:(e[0], e[1], "86", "Split", e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15], uuid.uuid1().int, e[17], e[18], e[19], e[20], e[21], e[22], e[23], e[24], e[25], e[26], e[27], e[28], e[29], e[30], e[31], e[32], e[33], e[34], e[35], e[36], e[37], e[38], e[39], e[40], e[41],e[42],e[43],e[44],e[45]))
toolbar_id_ods_event_data= ods_event_data.filter(lambda e:( e[20]=='6')).map(lambda e:(e[0], e[1], "87", "Toolbar ID", e[4], e[5], e[6], e[7], e[8], e[9], e[10], e[11], e[12], e[13], e[14], e[15], uuid.uuid1().int, e[17], e[18], e[19], e[20], e[21], e[22], e[23], e[24], e[25], e[26], e[27], e[28], e[29], e[30], e[31], e[32], e[33], e[34], e[35], e[36], e[37], e[38], e[39], e[40], e[41],e[42],e[43],e[44],e[45]))

ods_events=ods_event_data.union(browser_lang_ods_event_data).union(signup_email_ods_event_data).union(signup_bl_ods_event_data).union(signup_split_ods_event_data).union(login_split_ods_event_data).union(toolbar_id_ods_event_data)

for d in event_date.value:
    print d
    event_data.filter(lambda e:(e[3][:10])==d).map(lambda x:listToString(x)).coalesce(1, shuffle = True).saveAsTextFile(load_event_logs_dir+d)
    ods_events.filter(lambda e:(e[17][:10])==d).map(lambda x:listToString(x)).coalesce(1, shuffle = True).saveAsTextFile(load_event_details_dir+d)

today_pst=(pytz.timezone(targetTimeZone).localize(datetime.datetime.fromtimestamp(time.time())))
load_time=str(today_pst).replace("-","").replace(" ","_").replace(":","")

# Write raw password event logs to hdfs
passwordEventLogs.coalesce(1, shuffle = True).saveAsTextFile(load_raw_password_event_dir+'dealwalletPasswordEventData_'+load_time)
return_code=commands.getstatusoutput('hdfs dfs -mv ' + load_raw_password_event_dir + 'dealwalletPasswordEventData_* ' + raw_password_event_dir )
if return_code[0] <>0 :
    print "Error in moving raw password event logs to "+ raw_password_event_dir +" !!!"+ str(return_code)
    sys.exit(9999)

for d in event_date.value:
    cmd = 'hdfs dfs -mv '+load_event_logs_dir+d+"/part* " +ods_event_logs_dir+d+"_"+load_time
    print cmd
    commands.getstatusoutput(cmd)
    cmd = 'hdfs dfs -mv '+load_event_details_dir+d+"/part* " +ods_event_details_dir+d+"_"+load_time
    print cmd
    commands.getstatusoutput(cmd)


cal_month =load_time[:6].replace('-','')
cmd_file=base_dir+'/exe/dealwalletcom_event_log_archive_hdfs_files.sh'
return_code=commands.getstatusoutput('hdfs dfs -mkdir -p /user/eventlog/dealwallet.com/archive'+'_'+cal_month)
cmd="hdfs dfs -mv "+" ".join(files)+" /user/eventlog/dealwallet.com/archive"+'_'+cal_month

print cmd
print  "*********************************************************************"
print cmd
archive_cmd_file=open(cmd_file,'w')
archive_cmd_file.write(str(cmd))
archive_cmd_file.close()

cmd="sh "+cmd_file
print cmd
commands.getstatusoutput(cmd)

print "Number of records loaded:"
print event_data.count()
print "Job completed successfully"

sc.stop()

