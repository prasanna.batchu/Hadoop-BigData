#USAGE OF MEMBER.PY FILE
#spark-submit /home/engineering/passingArguments/member.py resultantPath targetPath

#importing the required packages
import smtplib
import json
from urllib2 import Request, urlopen
import sys
from pyspark import SparkContext
from pyspark import SparkConf
import os
import commands
from pyspark.sql import HiveContext
from hdfs3 import HDFileSystem
#import hipchat
import time
import ConfigParser

### Spark Configuration for executing the program ###
#***************************************************#

conf = (SparkConf().setMaster("local[*]").setAppName("comparision of two rdds").set("spark.executor.memory", "1g"))
sc = SparkContext(conf = conf)
sqlContext = HiveContext(sc)


#########################################################
# Checking the number of arguments passed at the runtime #
#########################################################


if len(sys.argv) != 3 and len(sys.argv) != 4 and len(sys.argv) != 5:
    print >> sys.stderr, "Usage: " + sys.argv[0] + " <resultantPath> <targetPath> [<limit> / <start value> <end value>]"
    exit(-1)

# Store the arguments into variables
####################################

if len(sys.argv) == 3:
    script, resultantPath, targetPath = sys.argv

if len(sys.argv) == 4:
    script, resultantPath, targePath, limit_count = sys.argv

if len(sys.argv) == 5:
    script, resultantPath, targetPath, start_value, end_value = sys.argv

originalTimeZone = "Etc/UTC"
targetTimeZone = "America/Los_Angeles"

#Configuration poperties reading

config = ConfigParser.RawConfigParser()
config.read('config.properties')
user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   Path_of_status_file = config.get('Properties','Path_of_status_file')



#comparison of dw original and target textfiles from the hdfs location

resultDF = sqlContext.read.parquet(resultantPath)
expectedDF = sqlContext.read.parquet(targetPath)

no_of_columns = len(resultDF.columns)
date = time.strftime("%d/%m/%Y")
#for comparing the number of partitions in the hdfs location
result_num_partitions = resultDF.rdd.getNumPartitions()
expected_num_partitions = expectedDF.rdd.getNumPartitions()

if result_num_partitions == expected_num_partitions:
   files_count = "2, Number of partitions test case, SUCCESS, "+date+", bigdata.dealwallet.com"
   print " "+resultantPath+" parquet files count is = "+str(result_num_partitions)+" "+targetPath+" parquet files count is = "+str(expected_num_partitions)+" "
else:
   files_count = "2, Number of partitions test case, FAILED, "+date+", bigdata.dealwallet.com"
   print " "+resultantPath+" parquet files count is = "+str(result_num_partitions)+" "+targetPath+" parquet files count is = "+str(expected_num_partitions)+" "
#   hipchat.hipchatMessage('@job3_auto_test : it is failed to compare the number of parquet files in two hdfs locations data')
   
#   sys.exit()

#Comparing the Parquet Files with sorting order of the data in files
resultdfparquet = sqlContext.read.parquet(resultantPath+'/part-r-00001*')
targetdfparquet = sqlContext.read.parquet(targetPath+'/part-r-00001*')

if resultdfparquet.select(resultdfparquet[8]).collect() == targetdfparquet.select(targetdfparquet[8]).collect():
   Sorting_Order = "4, Sorting_order test case, SUCCESS, "+date+", bigdata.dealwallet.com"
   print "parquet files data is in the sorting order"
else:
   Sorting_Order = "4, Sorting_order test case, FAILED, "+date+", bigdata.dealwallet.com"
   print "parquet files are not in the sorting order"
#   hipchat.hipchatMessage('@job3_auto_test : in hdfs /user/hue/target location parquet files data is not in the sorting order')
  
# comparison of Schema for the target data and result data 
if resultDF.schema == expectedDF.schema :
   schema_validation = "5, schema comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"
   print "schema validation for the target and result files are same"
else:
   schema_validation = "5, schema comparison test case, FAILED, "+date+", bigdata.dealwallet.com"
   print "schema validation for the target and rsult files are different"
#   hipchat.hipcatMessage('@job3_auto_test : in hdfs /user/hue/target location schema validaton is falied')

numPartCheck = 'SUCCESS'
for i in [1,100,250,360]:
   part_val = '{0:05}'.format(i)
   dfFileName = targetPath+"/part-r-"+part_val+"*"
   dfFileName2 = resultantPath+"/part-r-"+part_val+"*"
#   print dfFileName + "	   "+dfFileName2 
   dataFrame = sqlContext.read.parquet(dfFileName).collect()
   dataFrame2 = sqlContext.read.parquet(dfFileName2).collect()
   if len(dataFrame) != len(dataFrame2):
	numPartCheck = 'FAILED'
	
if numPartCheck == 'SUCCESS' :
	partition_per_file = "7, Number of records per partition comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"
else:
	partition_per_file = "7, Number of records per partition comparison test case, FAILED, "+date+", bigdata.dealwallet.com"
 
#   sys.exit()
#Configure the host name of the hdfs file system
hdfs = HDFileSystem(host='bigdata.dealwallet.com', port=8020)
size_comp_target = hdfs.du('/user/hue/target/', total=False, deep=False)
convert_list_size = list(size_comp_target)
convert_list_size.sort()
no_of_files = len(convert_list_size)

for i in range(no_of_files):
   if i not in[0,421]:
      size = size_comp_target[convert_list_size[i]]
      if 25000 < size < 40000:
         File_Size = "3, File size comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"
#         print "Size of file number "+ str(i) +" is "+ str(size)
      else:
         File_Size = "3, File size comparison test case, FAILED, "+date+", bigdata.dealwallet.com"
         print str(i)+" Nunber file sizes  are different and size = "+str(size)
#         hipchat.hipchatMessage(str(i)+' number File sizes in the location '+resultantPath+' are different and size = '+str(size))
         break
#         sys.exit(9999)

for i in range(no_of_columns):
   if i not in [6, 7, 41, 42, 96]:
       if resultDF.select(resultDF[i]).collect() == expectedDF.select(expectedDF[i]).collect():
          Data_Comparison ="1, Data comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"      
          junk_data = "6, Junk data comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"
          dw_member_shuffle = "8, dw member table index shuffle test case, SUCCESS, "+date+", bigdata.dealwallet.com"
          visit_index = "9, visit index shuffle comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"
          traffic_source_shuffle = "10, traffic source input data bug comparison test case, SUCCESS, "+date+", bigdata.dealwallet.com"
#          print "In "+resultantPath+" and "+targetPath+" "+str(i)+ "th column is as expected"
       else:
          Data_Comparison ="1, Data comparison test case, FAILED, "+date+", bigdata.dealwallet.com" 
          junk_data = "6, Junk data comparison test case, FAILED, "+date+", bigdata.dealwallet.com"
          dw_member_shuffle = "8, dw member table index shuffle test case, FAILED, "+date+", bigdata.dealwallet.com"
          visit_index = "9, visit index shuffle comparison test case, FAILED, "+date+", bigdata.dealwallet.com"
          traffic_source_shuffle = "10, traffic source input data bug comparison test case, FAILED, "+date+", bigdata.dealwallet.com"
          print "In data validation an error occured in column number "+str(i)+" from count starts with 0"
#          hipchat.hipchatMessage('In data validation an error occured in column number '+str(i)+" from count starts with 0")
	  break

file_name = open("/tmp/current_execution.txt", "r")
text_file_name = file_name.read().strip()
fo = open(Path_of_status_file+text_file_name+".status", "w")
fo.write(str(Data_Comparison)+"\n"+str(files_count)+"\n"+str(File_Size)+"\n"+str(Sorting_Order)+"\n"+str(schema_validation)+"\n"+str(junk_data)+"\n"+str(partition_per_file)+"\n"+str(dw_member_shuffle)+"\n"+str(visit_index)+"\n"+str(traffic_source_shuffle)+"\n");
print "successfully written the message"
# Close opend file
fo.close()


