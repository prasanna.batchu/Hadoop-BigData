#importing the required packages
import json
from urllib2 import Request, urlopen
import ConfigParser
import os

#sending a message as hipchat notification

config = ConfigParser.RawConfigParser()
config.read('config.properties')

user = os.environ["USER"]

if user not in ['engineering', 'root']:
   print "Invalid User!!! Please make sure that you have privilege to run this script!!!"
   sys.exit(1234)

else:
   Room_id = config.get('Properties','Room_id')
   Room_name = config.get('Properties','Room_name')
def hipchatMessage(message):
   V2TOKEN = 'dALuFF3hciryIf0mwPI98rgcVyHhoIwzUwC7NJ4z'
   url = 'https://api.hipchat.com/v2/room/%s/notification' % Room_id
   headers = {"content-type": "application/json","authorization": "Bearer %s" % V2TOKEN}
   datastr = json.dumps({'message': message,'color': 'yellow','message_format': 'html','notify': True})
   request = Request(url, headers=headers, data=datastr)
   uo = urlopen(request)
   rawresponse = ''.join(uo)
   uo.close()
   assert uo.code == 204
   print 'hipchat notification send successfully to '+Room_name
